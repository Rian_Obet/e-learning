<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\Models\User;

class AdminUserSeeder extends Seeder
{
    public function run(){
        User::insert([
            'id'       => 1,
            'name'     => 'Cuytamvan',
            'email'    => 'admin@gmail.com',
            'password' => bcrypt('admin123')
        ]);

        Permission::insert([
            [ 'id' => 1, 'name' => 'User create', 'guard_name' => 'web', ],
            [ 'id' => 2, 'name' => 'User edit', 'guard_name' => 'web', ],
            [ 'id' => 3, 'name' => 'User index', 'guard_name' => 'web', ],
            [ 'id' => 4, 'name' => 'User delete', 'guard_name' => 'web', ],
            [ 'id' => 5, 'name' => 'Role index', 'guard_name' => 'web', ],
            [ 'id' => 6, 'name' => 'Role create', 'guard_name' => 'web', ],
            [ 'id' => 7, 'name' => 'Role edit', 'guard_name' => 'web', ],
            [ 'id' => 8, 'name' => 'Role delete', 'guard_name' => 'web', ],
            [ 'id' => 9, 'name' => 'Role configure', 'guard_name' => 'web', ],
            [ 'id' => 10, 'name' => 'Permission index', 'guard_name' => 'web', ],
            [ 'id' => 11, 'name' => 'Permission create', 'guard_name' => 'web', ],
            [ 'id' => 12, 'name' => 'Permission edit', 'guard_name' => 'web', ],
            [ 'id' => 13, 'name' => 'Permission delete', 'guard_name' => 'web', ],
            [ 'id' => 14, 'name' => 'User configure', 'guard_name' => 'web', ]
        ]);

        Role::insert([
            ['id' => 1, 'name' => 'Super Admin', 'guard_name' => 'web']
        ]);

        DB::table('role_has_permissions')->insert([
            [ 'permission_id' => 1, 'role_id' => 1 ],
            [ 'permission_id' => 2, 'role_id' => 1 ],
            [ 'permission_id' => 3, 'role_id' => 1 ],
            [ 'permission_id' => 4, 'role_id' => 1 ],
            [ 'permission_id' => 5, 'role_id' => 1 ],
            [ 'permission_id' => 6, 'role_id' => 1 ],
            [ 'permission_id' => 7, 'role_id' => 1 ],
            [ 'permission_id' => 8, 'role_id' => 1 ],
            [ 'permission_id' => 9, 'role_id' => 1 ],
            [ 'permission_id' => 10, 'role_id' => 1 ],
            [ 'permission_id' => 11, 'role_id' => 1 ],
            [ 'permission_id' => 12, 'role_id' => 1 ],
            [ 'permission_id' => 13, 'role_id' => 1 ],
            [ 'permission_id' => 14, 'role_id' => 1 ]
        ]);

        DB::table('model_has_roles')->insert([
            [ 'role_id' => 1, 'model_type' => 'App\\Models\\User', 'model_id' => 1 ],
        ]);
    }
}
