<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableNames = config('question.table_names');

        if (empty($tableNames)) {
            throw new \Exception('Error: config/permission.php not found and defaults could not be merged. Please publish the package configuration before proceeding.');
        }

        Schema::create('question_packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('subject_id');
            $table->timestamps();
        });

        Schema::create($tableNames['question_has_packages'], function (Blueprint $table) use ($tableNames) {
            $table->unsignedBigInteger('question_id');
            $table->unsignedBigInteger('package_id');

            $table->foreign('question_id')
                ->references('id')
                ->on($tableNames['questions'])
                ->onDelete('cascade');

            $table->foreign('package_id')
                ->references('id')
                ->on($tableNames['question_packages'])
                ->onDelete('cascade');

            $table->primary(['question_id', 'package_id'], 'question_has_packages_package_id_question_id_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_packages');
    }
}
