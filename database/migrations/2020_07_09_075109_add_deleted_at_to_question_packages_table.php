<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDeletedAtToQuestionPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('question_packages', function (Blueprint $table) {
            $table->softDeletes()->after('updated_at');
            $table->string('created_by', 100)->nullable()->after('deleted_at');
            $table->string('updated_by', 100)->nullable()->after('created_by');
            $table->string('deleted_by', 100)->nullable()->after('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('question_packages', function (Blueprint $table) {
            //
        });
    }
}
