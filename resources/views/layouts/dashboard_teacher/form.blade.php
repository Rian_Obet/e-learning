<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
  <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
  <meta name="author" content="{{ config('app.author') }}">
  <title>{{ config('app.name') }} {{ isset($title) ? '| '.$title : '' }}</title>
  <link rel="apple-touch-icon" href="{{ asset('dashboard/images/ico/apple-icon-120.png') }}">
  <link rel="shortcut icon" type="image/x-icon" href="{{ asset('dashboard/images/ico/favicon.ico') }}">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700" rel="stylesheet">
  <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css"
  rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ asset('dashboard/css/vendors.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('dashboard/vendors/css/forms/icheck/icheck.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('dashboard/vendors/css/forms/icheck/custom.css') }}">
  @yield('vendor-style')
  @stack('vendor-style')
  <link rel="stylesheet" type="text/css" href="{{ asset('dashboard/css/app.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('dashboard/css/core/menu/menu-types/vertical-content-menu.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('dashboard/css/core/colors/palette-gradient.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('dashboard/css/pages/login-register.css') }}">
  @yield('style')
  @stack('style')
</head>
<body class="vertical-layout vertical-content-menu 1-column menu-expanded blank-page blank-page" data-open="click" data-menu="vertical-content-menu" data-col="1-column">
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-body">
        <section class="flexbox-container">
          <div class="col-12 d-flex align-items-center justify-content-center">
            <div class="col-md-4 col-10 box-shadow-2 p-0">
              <div class="card border-grey border-lighten-3 m-0">
                <div class="card-header border-0">
                  <div class="card-title text-center">
                    <div class="p-1">
                      <img src="{{ asset('dashboard/images/logo/logo-dark.png') }}" alt="branding logo">
                    </div>
                  </div>
                  @isset($subtitle)
                  <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2">
                    <span>{{ $subtitle }}</span>
                  </h6>
                  @endisset
                </div>
                <div class="card-content">
                  <div class="card-body">
                    @yield('content')
                  </div>
                </div>
                <div class="card-footer">
                  @yield('content-footer')
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>

  <script src="{{ asset('dashboard/vendors/js/vendors.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('dashboard/vendors/js/ui/headroom.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('dashboard/vendors/js/forms/icheck/icheck.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('dashboard/vendors/js/forms/validation/jqBootstrapValidation.js') }}" type="text/javascript"></script>

  <script src="{{ asset('vendor/sweetalert/sweetalert.all.js')  }}"></script>
  @include('sweetalert::alert')
  @include('sweetalert::validation')

  @yield('vendor-script')
  @stack('vendor-script')
  <script src="{{ asset('dashboard/js/core/app-menu.js') }}" type="text/javascript"></script>
  <script src="{{ asset('dashboard/js/core/app.js') }}" type="text/javascript"></script>
  <script src="{{ asset('dashboard/js/scripts/forms/form-login-register.js') }}" type="text/javascript"></script>
  @yield('script')
  @stack('script')
</body>
</html>