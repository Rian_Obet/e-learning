@php
  $auth_user = Auth::guard('student')->user();
@endphp
<nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-light navbar-hide-on-scroll navbar-border navbar-shadow navbar-brand-center">
  <div class="navbar-wrapper">
    <div class="navbar-header">
      <ul class="nav navbar-nav flex-row">
        <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
        <li class="nav-item">
          <a class="navbar-brand" href="{{ '' }}">
            <img class="brand-logo" alt="modern admin logo" src="{{ asset('dashboard/images/logo/logo.png') }}">
            <h3 class="brand-text">{{ config('app.name') }}</h3>
          </a>
        </li>
        <li class="nav-item d-md-none">
          <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a>
        </li>
      </ul>
    </div>
    <div class="navbar-container content">
      <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="nav navbar-nav mr-auto float-left">
          <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
        </ul>
        <ul class="nav navbar-nav float-right">
          <li class="dropdown dropdown-user nav-item">
            <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
              <span class="mr-1">Hello,
                <span class="user-name text-bold-700">{{ $auth_user->name }}</span>
              </span>
              <span class="avatar avatar-online">
                <img src="{{ $auth_user->img_url('245') }}" alt="avatar">
                <i></i>
              </span>
            </a>
            <form action="{{ route('student.logout') }}" method="post" id="form-logout" class="d-none">@csrf</form>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" href="{{ route('student.index') }}"><i class="ft-user"></i> Profile</a>
              {{-- <a class="dropdown-item" href="{{ route('admin.activity-log.index') }}"><i class="ft-check-square"></i> Activity Log</a> --}}
              <div class="dropdown-divider"></div>
              <a class="dropdown-item logout-link" href="#"><i class="ft-power"></i> Logout</a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</nav>
