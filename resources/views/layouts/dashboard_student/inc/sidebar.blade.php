<div class="main-menu menu-static menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
      @php
        $item = [
          'text' => 'Dashboard',
          'url'  => 'student.index',
          'icon' => 'la la-dashboard',
        ];
        $menu = new MenuItem($item['text'], $item['url'], $item['icon']);
        $menu->isRoute();
      @endphp
      @include('layouts.dashboard_student.inc.sidebar-item', ['data' => $menu])


  
        <li class="navigation-header center">
            <span>Ujian</span>
            <i class="la la-ellipsis-h ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="Support"></i>
        </li>


        
{{--       
        @php
          $item = [
            'text' => 'Daftar Ujian',
            'url'  => 'student.examination.index',
            'icon' => 'la la-book',
          ];
          $menu = new MenuItem($item['text'], $item['url'], $item['icon']);
          $menu->isRoute();
        @endphp
        @include('layouts.dashboard_student.inc.sidebar-item', ['data' => $menu])
     --}}
    </ul>
  </div>
</div>
