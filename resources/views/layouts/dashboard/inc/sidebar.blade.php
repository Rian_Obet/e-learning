<div class="main-menu menu-static menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
      @php
        $item = [
          'text' => 'Dashboard',
          'url'  => 'admin.index',
          'icon' => 'la la-dashboard',
        ];
        $menu = new MenuItem($item['text'], $item['url'], $item['icon']);
        $menu->isRoute();
      @endphp
      @include('layouts.dashboard.inc.sidebar-item', ['data' => $menu])


      @php
        $permissions = [
          'User index',
          'Permission index',
          'Role index',
        ];
      @endphp
      @canany($permissions)
        <li class="navigation-header">
            <span>Management</span>
            <i class="la la-ellipsis-h ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="Support"></i>
        </li>

        @can('Teacher index')
        @php
          $item = [
            'text' => 'Manajemen Guru',
            'url'  => 'admin.teacher.index',
            'icon' => 'la la-user',
          ];
          $menu = new MenuItem($item['text'], $item['url'], $item['icon']);
          $menu->isRoute();
        @endphp
        @include('layouts.dashboard.inc.sidebar-item', ['data' => $menu])
        @endcan

        @can('Student index')
        @php
          $item = [
            'text' => 'Manajemen Siswa',
            'url'  => 'admin.student.index',
            'icon' => 'la la-user',
          ];
          $menu = new MenuItem($item['text'], $item['url'], $item['icon']);
          $menu->isRoute();
        @endphp
        @include('layouts.dashboard.inc.sidebar-item', ['data' => $menu])
        @endcan

        @can('Examination index')
        @php
          $item = [
            'text' => 'Manajemen Ujian (Paket Soal)',
            'url'  => 'admin.examination.index',
            'icon' => 'la la-book',
          ];
          $menu = new MenuItem($item['text'], $item['url'], $item['icon']);
          $menu->isRoute();
        @endphp
        @include('layouts.dashboard.inc.sidebar-item', ['data' => $menu])
        @endcan

        @can('Question index')
        @php
          $item = [
            'text' => 'Bank Soal',
            'url'  => 'admin.question.index',
            'icon' => 'la la-book',
          ];
          $menu = new MenuItem($item['text'], $item['url'], $item['icon']);
          $menu->isRoute();
        @endphp
        @include('layouts.dashboard.inc.sidebar-item', ['data' => $menu])
        @endcan

        @can('Question_Package index')
        @php
          $item = [
            'text' => 'Paket Soal',
            'url'  => 'admin.question_package.index',
            'icon' => 'la la-book',
          ];
          $menu = new MenuItem($item['text'], $item['url'], $item['icon']);
          $menu->isRoute();
        @endphp
        @include('layouts.dashboard.inc.sidebar-item', ['data' => $menu])
        @endcan

        @can('Subject index')
        @php
          $item = [
            'text' => 'Mata Pelajaran',
            'url'  => 'admin.subject.index',
            'icon' => 'la la-book',
          ];
          $menu = new MenuItem($item['text'], $item['url'], $item['icon']);
          $menu->isRoute();
        @endphp
        @include('layouts.dashboard.inc.sidebar-item', ['data' => $menu])
        @endcan

        @can('Class index')
        @php
          $item = [
            'text' => 'Kelas',
            'url'  => 'admin.class.index',
            'icon' => 'la la-book',
          ];
          $menu = new MenuItem($item['text'], $item['url'], $item['icon']);
          $menu->isRoute();
        @endphp
        @include('layouts.dashboard.inc.sidebar-item', ['data' => $menu])
        @endcan

        <li class="navigation-header">
          <span>User</span>
          <i class="la la-ellipsis-h ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="Support"></i>
        </li>

        @can('User index')
        @php
          $item = [
            'text' => 'User',
            'url'  => 'admin.user.index',
            'icon' => 'la la-user',
          ];
          $menu = new MenuItem($item['text'], $item['url'], $item['icon']);
          $menu->isRoute();
        @endphp
        @include('layouts.dashboard.inc.sidebar-item', ['data' => $menu])
        @endcan

        @canany(['Permission index', 'Role index'])
        @php
          $item = [
            'text' => 'Permissions',
            'url'  => '',
            'icon' => 'la la-shield',
          ];
          $menu = new MenuItem($item['text'], $item['url'], $item['icon']);
        @endphp

        @can('Permission index')
          @php $menu->add('Permission', 'admin.permission.index')->isRoute(); @endphp
        @endcan

        @can('Role index')
          @php $menu->add('Role', 'admin.role.index')->isRoute(); @endphp
        @endcan

        @include('layouts.dashboard.inc.sidebar-item', ['data' => $menu])
        @endcanany
      @endcanany
    </ul>
  </div>
</div>
