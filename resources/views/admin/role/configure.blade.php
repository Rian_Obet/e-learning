@extends('layouts.dashboard.app')

@section('content')
<div class="row">
  <div class="col-sm-12 col-md-6">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Configure {{ $title }} : <span class="badge badge-info">{{ $data->name }}</span> </h4>
        <a class="heading-elements-toggle"><i class="ft-align-justify font-medium-3"></i></a>
        <div class="heading-elements">
          <ul class="list-inline mb-0">
            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
            <li><a class="check-all" title="Check all permission on this page"><i class="ft-check"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="card-content collapse show">
        <div class="card-body">
          <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-4 offset-md-6 offset-md-8">
              <div class="form-group pr-1">
                <input type="text" class="form-control ml-1" id="search-permission" autocomplete="off" placeholder="Search Permission...">
              </div>
            </div>
          </div>
          <form action="{{ route($route.'configure.action', $id) }}" method="post">
            @csrf
            <input type="hidden" name="permissions">
            <input type="hidden" name="delete_permissions" id="dltPermission" value="[]">
            <table class="table table-bordered" id="dataTable">
              <thead>
                <tr>
                  <th style="width: 30px">No</th>
                  <th>Permission</th>
                  <th style="width: 30px">Check</th>
                </tr>
              </thead>
              <tbody id="data-permission">
                @php($no = 1)
                @foreach ($permissions as $r)
                <tr>
                  <td>{{ $no }}.</td>
                  <td>{{ $r->name }}</td>
                  <td>
                    <input type="checkbox" name="permissions[]" class="check-permissions {{ in_array($r->id, $isPermission) ? 'is-permission' : '' }}" value="{{ $r->id }}" {{ in_array($r->id, $isPermission) ? 'checked' : '' }}>
                  </td>
                </tr>
                @php($no++)
                @endforeach
              </tbody>
            </table>
            <div class="text-right pr-1">
              <a href="{{ route($route.'index') }}" class="btn btn-outline-warning"><i class="ft-arrow-left"></i> Back</a>
              <button class="btn btn-outline-primary"><i class="ft-check"></i> Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
  $(document).ready(function () {
    $('.is-permission').change(function(){
      let id = $(this).val();
      let dlt = $('#dltPermission');
      let data = JSON.parse(dlt.val());

      data = (!this.checked) ? [...data, id] : data.filter(r => r != id);
      dlt.val(JSON.stringify(data));
    })

    $('.check-all').click(function(e){
      e.preventDefault();
      $('.check-permissions').attr('checked', true);
    })

    $('#search-permission').keyup(function(){
      let _value = $(this).val().toUpperCase();
      let data = $('#data-permission').find('tr');
      data.map((k, v) => {
        let text = $($(v).find('td')[1]).text();
        if(text.toUpperCase().indexOf(_value) > -1){
          $(v).css({display: ''});
        }else{
          $(v).css({display: 'none'});
        }
      })
    })
  });
</script>
@endsection
