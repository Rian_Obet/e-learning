<div class="row">
  <div class="form-group col-md-12">
    <label>Name</label>
    <input type="text" name="name" class="form-control" placeholder="Name" autocomplete="off" value="{{ old('name', (isset($isEdit) ? $data->name : '')) }}" required>
  </div>
</div>
