<div class="row">
    <div class="form-group col-md-6">
      <label>Nama</label>
      <input type="text" name="name" class="form-control" placeholder="Name" autocomplete="off" value="{{ old('name', (isset($isEdit) ? $data->name : '')) }}">
    </div>
    <div class="form-group col-md-6">
      <label>Nis</label>
      <input type="number" name="nis" class="form-control" placeholder="Nis" autocomplete="off" value="{{ old('nis', (isset($isEdit) ? $data->nis : '')) }}">
    </div>
    <div class="form-group col-sm-12">
        <label>Jenis Kelamin</label>
        <br>
        <div class="input-group">
          <div class="icheck-inline">
            <label class="hover">
              <div class="iradio_minimal-grey" style="position:relative;">
                <input type="radio" name="gender" class="icheck" style="" value="L"
                  {{ isset($isEdit) && $data->gender == 'L' ? 'checked' : '' }} required autocomplete="off"> &nbsp; Laki
                Laki &nbsp; &nbsp;
              </div>
            </label>
            <label class="hover">
              <div class="iradio_minimal-grey" style="">
                <input type="radio" name="gender" class="icheck" style="" value="P"
                  {{ isset($isEdit) && $data->gender == 'P' ? 'checked' : '' }} required autocomplete="off"> &nbsp;
                Perempuan
              </div>
            </label>
          </div>
        </div>
    </div>
    <div class="form-group col-md-6">
        <label>Email</label>
        <input type="email" name="email" class="form-control" placeholder="Email" autocomplete="off" value="{{ old('email', (isset($isEdit) ? $data->email : '')) }}">
      </div>
    <div class="form-group col-md-6">
      <label>Password</label>
      <input type="password" name="password" class="form-control" placeholder="*******" >
    </div>
    <div class="form-group col-sm-12">
        <label>Kelas</label>
        <select name="class_id" id="class_id" class="form-control select2">
          <option value="">Pilih Kelas</option>
          @if (count($class) > 0)
          @foreach ($class as $c)
          <option value="{{ $c->id }}" {{ (isset($isEdit) && $data->class_id == $c->id) ? 'selected' : '' }}>{{ $c->name }}
          </option>
          @endforeach
          @else
          <option value="" selected disabled>Tidak ada data</option>
          @endif
        </select>
      </div>
    </div>
    <div class="form-group col-md-12">
        <label>Image</label>
        <div>
          <input type="file" accept="image/*" name="image" id="image">
        </div>
      </div>
  </div>
