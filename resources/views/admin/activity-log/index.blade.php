@extends('layouts.dashboard.app')

@section('style')
  <link rel="stylesheet" href="{{ asset('dashboard/css/core/colors/palette-callout.css') }}">
@endsection

@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="card" id="card-activity">
      <div class="card-header">
        <h4 class="card-title">{{ $title }}</h4>
        <a class="heading-elements-toggle"><i class="ft-align-justify font-medium-3"></i></a>
      </div>
      <div class="card-content">
        <div class="card-body">
          <div class="text-right pr-1">
            
          </div>
          <table class="table table-bordered table-xs">
            <thead>
              <tr>
                <th style="width: 50px">#</th>
                <th>Activity</th>
                <th>Causer</th>
                <th>Create At</th>
              </tr>
            </thead>
            <tbody id="dataTable">
              <tr>
                <td colspan="4" class="text-center">Loading data...</td>
              </tr>
            </tbody>
          </table>

          <div class="text-center">
            <button id="load-data" class="btn btn-outline-primary round">Load data</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
  $(document).ready(function () {
    var url = '{{ route($route.'api') }}';
    var next_url = null;
    var no = 0;
    var type = {
      create: 'bs-callout-info',
      delete: 'bs-callout-danger',
      edit: 'bs-callout-success',
      configure: 'bs-callout-primary'
    }

    loadActivities(url);
    function loadActivities(url){
      $.ajax({
        url     : url,
        dataType: "json",
        beforeSend: function(){
          $('#card-activity')
        },
        success : function (res) {
          next_url = res.next_page;
          if(next_url === null) $('#load-data').remove();
          if(no == 0){
            $('#dataTable').html('');
            no++;
          }

          let html = '';
          res.data.forEach(r => {
            html += `<tr>
                <td>${no}.</td>
                <td>
                  <p><strong>${r.properties.title ? r.properties.title : r.description }</strong> : <small>${r.description}</small></p>
                </td>
                <td>${r.causer ? r.causer.name : '-'}</td>
                <td>${r.created_at}</td>
              </tr>`
              no++;
          });
          $('#dataTable').append(html);
        }
      });
    }
    $('#load-data').click(function(e){
      e.preventDefault();
      loadActivities(next_url);
    })
  });
</script>

@endsection