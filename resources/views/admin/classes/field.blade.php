<div class="row">
  <div class="form-group col-md-12">
    <label>Class</label>
    <input type="text" name="name" class="form-control" placeholder="Class" autocomplete="off" value="{{ old('name', (isset($isEdit) ? $data->name : '')) }}" required>
  </div>

  {{-- @if (!isset($isEdit))
    <div class="form-group col-sm-12">
      <label>
        <input type="checkbox" name="crud"> Crud
      </label>
    </div>
  @endif --}}
</div>
