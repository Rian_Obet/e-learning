@extends('layouts.dashboard.app')

@section('content')
<div class="row">
  <div class="col-md-4">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Detail {{ $title }}</h4>
        <a class="heading-elements-toggle"><i class="ft-align-justify font-medium-3"></i></a>
        <div class="heading-elements">
          <ul class="list-inline mb-0">
            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="card-content collapse show">
        <div class="card-body">
          <div class="row form-group">
            <div class="col-3">
              <label>ID Paket Soal</label>
            </div>
            <div class="col-9">
              <input type="text" value="{{ $exam->id }}" disabled class="form-control">
            </div>
          </div>
          <div class="row form-group">
            <div class="col-3">
              <label>Nama Paket Soal</label>
            </div>
            <div class="col-9">
              <input type="text" value="{{ $exam->name }}" disabled class="form-control">
            </div>
          </div>
          <div class="row form-group">
            <div class="col-3">
              <label>Jumlah Soal</label>
            </div>
            <div class="col-9">
              <input type="text" value="{{ $exam->question_total }}" disabled class="form-control">
            </div>
          </div>
          <div class="row form-group">
            <div class="col-3">
              <label>Tipe Soal</label>
            </div>
            <div class="col-9">
              <input type="text" value="{{ $exam->type }}" disabled class="form-control">
            </div>
          </div>
          <div class="text-right">
            <a href="{{ route($route.'index') }}" class="btn btn-outline-primary ml-1"> <i class="ft-arrow-left"></i> Back</a>
          </div>
        </div>
      </div>
    </div>
  </div>

{{-- end --}}

  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Tambah Soal</h4>
        <a class="heading-elements-toggle"><i class="ft-align-justify font-medium-3"></i></a>
        <div class="heading-elements">
          <ul class="list-inline mb-0">
            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
          </ul>
        </div>
      </div>

      <div class="card-content collapse show">
        <div class="card-body">
          <form action="{{ route($route.'save-question-examination') }}" method="post">
                @csrf
            <div class="form-body">
            <div class="form-group col-md-12">
                <label for="teacher_id">Guru</label>
                {{-- <input type="hidden" name="id" value="N"> --}}
                <input type="hidden" name="examination_id" value="{{ $exam->id }}">
                <select name="teacher_id" class="form-control">
                  {{-- <option value="" disabled selected>--- Pilih ---</option> --}}
                        {{-- @if (count($teachers) > 0) --}}
                        @foreach ($teachers as $r)
                        <option value="{{ $r->id }}" {{ (isset($isEdit) && $data->teacher_id == $r->id) ? 'selected' : '' }}>{{ $r->name }}</option>
                        @endforeach
                            {{-- @else
                            <option value="" selected disabled>Tidak ada data</option>
                            @endif --}}
                </select>
                <small class="help-block"></small>
              </div>
                <div class="form-group col-md-12">
                  <label for="subject_id">Mata Pelajaran</label>
                  <select name="subject_id" class="form-control">
                    {{-- <option value="" disabled selected>--- Pilih ---</option> --}}
                    @if (count($subjects) > 0)
                    @foreach ($subjects as $r)
                    <option value="{{ $r->id }}" {{ (isset($isEdit) && $data->subject_id == $r->id) ? 'selected' : '' }}>{{ $r->name }}</option>
                    @endforeach
                    @else
                    <option value="" selected disabled>Tidak ada data</option>
                    @endif
                  </select>
                  <small class="help-block"></small>
                </div>
              <div class="form-group col-md-12">
                <label>Soal</label>
                <textarea name="question" class="form-control" id="ckeditors" placeholder="soal" autocomplete="off" value="{{ old('question', (isset($isEdit) ? $data->question : '')) }}" cols="30" rows="10" required></textarea>
                {{-- <input type="text" name="question" class="form-control" id="ckeditors" placeholder="soal" autocomplete="off" value="{{ old('question', (isset($isEdit) ? $data->question : '')) }}" required> --}}
              </div>
              <div class="form-group col-md-12">
                <label>Jawaban A</label>
                <textarea name="select1" id="ckeditors1" class="form-control" placeholder="jumlah soal" autocomplete="off" value="{{ old('select1', (isset($isEdit) ? $data->select1 : '')) }}" required cols="30" rows="10"></textarea>
                {{-- <input type="text" name="select1" class="form-control" id="ckeditors1" placeholder="jumlah soal" autocomplete="off" value="{{ old('select1', (isset($isEdit) ? $data->select1 : '')) }}" required> --}}
              </div>
              <div class="form-group col-md-12">
                <label>Jawaban B</label>
                <textarea name="select2" id="ckeditors2" class="form-control" placeholder="jumlah soal" autocomplete="off" value="{{ old('select2', (isset($isEdit) ? $data->select2 : '')) }}" required cols="30" rows="10"></textarea>
                {{-- <input type="text" name="select2" class="form-control" id="ckeditors2" placeholder="tanggal mulai" autocomplete="off" value="{{ old('select2', (isset($isEdit) ? $data->select2 : '')) }}" required> --}}
              </div>
              <div class="form-group col-md-12">
                <label>Jawaban C</label>
                <textarea name="select3" id="ckeditors3" class="form-control" placeholder="jumlah soal" autocomplete="off" value="{{ old('select3', (isset($isEdit) ? $data->select3 : '')) }}" required cols="30" rows="10"></textarea>
                {{-- <input type="text" name="select3" class="form-control" id="ckeditors3" placeholder="tanggal selesai" autocomplete="off" value="{{ old('select3', (isset($isEdit) ? $data->select3 : '')) }}" required> --}}
              </div>
              <div class="form-group col-md-12">
                <label>Jawaban D</label>
                <textarea name="select4" id="ckeditors4" class="form-control" placeholder="jumlah soal" autocomplete="off" value="{{ old('select4', (isset($isEdit) ? $data->select4 : '')) }}" required cols="30" rows="10"></textarea>
                {{-- <input type="text" name="select4" class="form-control" id="ckeditors4" placeholder="waktu ujian" autocomplete="off" value="{{ old('select4', (isset($isEdit) ? $data->select4 : '')) }}" required> --}}
              </div>
              <div class="form-group col-md-12">
                <label>Jawaban E</label>
                <textarea name="select5" id="ckeditors5" class="form-control" placeholder="jumlah soal" autocomplete="off" value="{{ old('select5', (isset($isEdit) ? $data->select5 : '')) }}" required cols="30" rows="10"></textarea>
                {{-- <input type="text" name="select5" class="form-control" id="ckeditors5" placeholder="tanggal selesai" autocomplete="off" value="{{ old('select5', (isset($isEdit) ? $data->select5 : '')) }}" required> --}}
              </div>
              <div class="form-group col-md-12">
                <label for="">Kunci Jawaban</label>
                <select name="answer" class="form-control">
                  @foreach ($abjad as $item)
                  <option value="{{ $item }}" {{ (isset($isEdit) && $data->answer == $item) ? 'selected' : '' }}>{{ $item }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group col-md-12">
                <label for="">Bobot Soal</label>
                <input type="number" name="weight" id="" class="form-control" placeholder="bobot soal" autocomplete="off" value="{{ old('weight', (isset($isEdit) ? $data->weight : '')) }}" required>
              </div>
            </div>
              <div class="form-actions">
                <div class="text-right">
                  <button type="reset" class="btn btn-outline-warning"> <i class="ft-refresh-cw"></i> Reset</button>
                  <button type="submit" class="btn btn-outline-primary ml-1"> <i class="ft-check"></i> Save</button>
                </div>
              </div>
            </form>
        </div>
      </div>

    </div>

    <div class="card">
        <div class="card-header">
        <h4 class="card-title">List Soal</h4>
        <a class="heading-elements-toggle"><i class="ft-align-justify font-medium-3"></i></a>
        <div class="heading-elements">
          <ul class="list-inline mb-0">
            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="card-content collapse show">
        <div class="card-body">
          <div class="text-right pr-1">

          </div>
          <table class="table table-bordered" id="dataTable">
            <thead>
              <tr>
                <th>Soal</th>
                <th>Kunci Jawaban</th>
                <th>Score</th>
                <th>Tanggal Dibuat</th>
                <th style="width: 100px">Action</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-6">
    <table></table>
  </div>
</div>

{{-- <div class="row">
    tes
</div> --}}
@endsection

@section('vendor-script')
<script src="{{ asset('dashboard/vendors/js/tables/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('dashboard/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
@endsection

@section('script')
<script src="{{ asset('dashboard/custom/js/datatable.js') }}"></script>
<script>
      $(document).ready(function () {
        const url = '{{ route($route.'get-detail-soal') }}';

    dataSourceServerSide.init('#dataTable', {
      url: url,
      column: [
        {data: 'question'},
        {data: 'answer'},
        {data: 'weight'},
        {data: 'created_at'},

        {data: 'id', render: function(data, type, full, meta) {
          let action = '';

          action += `<a class="dropdown-item" href="${url}/${data}/edit"><i class="la la-edit"></i> Edit</a>`

          action += `<form action="${url}/${data}" method="post" class="form-inline">@method('DELETE') @csrf<button class="dropdown-item delete-from-table"><i class="la la-trash"></i> Delete</button></form>`

          let btn = `<span class="dropdown">
            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
              <i class="la la-ellipsis-h"></i>
            </a>
            <div class="dropdown-menu">${action}</div>
          </span>`
          return btn;
        }},
      ]
    });
  });

  CKEDITOR.replace( 'ckeditors', {
    filebrowserUploadUrl: "{{route('admin.post.image', ['_token' => csrf_token() ])}}",
    filebrowserUploadMethod: 'form'
  });
  CKEDITOR.replace( 'ckeditors1' );
  CKEDITOR.replace( 'ckeditors2' );
  CKEDITOR.replace( 'ckeditors3' );
  CKEDITOR.replace( 'ckeditors4' );
  CKEDITOR.replace( 'ckeditors5' );
  CKEDITOR.on('instanceLoaded', function(e) {e.editor.resize(600, 200)} );
  </script>
@endsection
