<div class="row">
  <div class="form-group col-md-6">
    <label>Name</label>
    <input type="text" name="name" class="form-control" placeholder="Name" autocomplete="off" value="{{ old('name', (isset($isEdit) ? $data->name : '')) }}">
  </div>
  <div class="form-group col-md-6">
    <label>Email</label>
    <input type="email" name="email" class="form-control" placeholder="Email" autocomplete="off" value="{{ old('email', (isset($isEdit) ? $data->email : '')) }}" {{ isset($isEdit) && $isEdit ? 'disabled' : '' }}>
  </div>
  <div class="form-group col-md-6">
    <label>Password</label>
    <input type="password" name="password" class="form-control" placeholder="*******">
  </div>
  <div class="form-group col-md-6">
    <label>Password Confirmation</label>
    <input type="password" name="password_confirmation" class="form-control" placeholder="*******">
  </div>
  <div class="form-group col-md-12">
    <label>Image</label>
    <div>
      <input type="file" accept="image/*" name="image" id="image">
    </div>
  </div>
</div>
