<div class="row">
    <div class="form-group col-md-6">
      <label>Nama</label>
      <input type="text" name="name" class="form-control" placeholder="Name" autocomplete="off" value="{{ old('name', (isset($isEdit) ? $data->name : '')) }}">
    </div>
    <div class="form-group col-md-6">
      <label>Nik</label>
      <input type="number" name="reg_number" class="form-control" placeholder="Nik" autocomplete="off" value="{{ old('reg_number', (isset($isEdit) ? $data->reg_number : '')) }}">
    </div>
    <div class="form-group col-sm-12">
        <label>Jenis Kelamin</label>
        <br>
        <div class="input-group">
          <div class="icheck-inline">
            <label class="hover">
              <div class="custom-control custom-radio pmd-radio custom-control-inline" style="position:relative;">
                <input type="radio" name="gender" id="customRadioInline2" class="custom-control-input" style="" value="L"
                  {{ isset($isEdit) && $data->gender == 'L' ? 'checked' : '' }} required autocomplete="off"> &nbsp;
                   <label class="custom-control-label pmd-radio-ripple-effect" for="customRadioInline2">Laki Laki</label> &nbsp; &nbsp;
              </div>
            </label>
            <label class="">
              <div class="custom-control custom-radio pmd-radio custom-control-inline" style="">
                <input type="radio" name="gender" id="customRadioInline1" class="custom-control-input" style="" value="P"
                  {{ isset($isEdit) && $data->gender == 'P' ? 'checked' : '' }} required autocomplete="off"> &nbsp;
                  <label class="custom-control-label pmd-radio-ripple-effect" for="customRadioInline1">Perempuan</label>
              </div>
            </label>
          </div>
        </div>
    </div>
    <div class="form-group col-md-6">
        <label>Email</label>
        <input type="email" name="email" class="form-control" placeholder="Email" autocomplete="off" value="{{ old('email', (isset($isEdit) ? $data->email : '')) }}">
      </div>
    <div class="form-group col-md-6">
      <label>Password</label>
      <input type="password" name="password" class="form-control" placeholder="*******" >
    </div>
    <div class="form-group col-sm-12">
        <label>Mata Pelajaran</label>
        <select name="subject_id" id="subject_id" class="form-control select2">
          @if (count($subject) > 0)
          @foreach ($subject as $s)
          <option value="{{ $s->id }}" {{ (isset($isEdit) && $data->subject_id == $s->id) ? 'selected' : '' }}>{{ $s->name }}
          </option>
          @endforeach
          @else
          <option value="" selected disabled>Tidak ada data</option>
          @endif
        </select>
      </div>
    </div>
    <div class="form-group col-md-12">
        <label>Image</label>
        <div>
          <input type="file" accept="image/*" name="image" id="image">
        </div>
      </div>
  </div>
