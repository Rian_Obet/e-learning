@extends('layouts.dashboard.app')

@section('vendor-style')
  <link rel="stylesheet" href="{{ asset('dashboard/vendors/css/tables/datatable/datatables.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dashboard/vendors/css/tables/extensions/responsive.dataTables.min.css') }}">
@endsection

@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">{{ $title }}</h4>
        <a class="heading-elements-toggle"><i class="ft-align-justify font-medium-3"></i></a>
        <div class="heading-elements">
          @can($permission.'create')
          <a href="{{ route($route.'create') }}" class="btn btn-primary btn-glow">Create</a>
          @endcan
        </div>
      </div>
      <div class="card-content collapse show">
        <div class="card-body">
          <div class="text-right pr-1">
            
          </div>
          <table class="table table-bordered" id="dataTable">
            <thead>
              <tr>
                <th>Nama</th>
                <th>Nik</th>
                <th>Jenis Kelamin</th>
                <th>Email</th>
                <th>Mata Pelajaran</th>
                <th style="width: 100px">Aksi</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('vendor-script')
<script src="{{ asset('dashboard/vendors/js/tables/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('dashboard/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
@endsection

@section('script')
<script src="{{ asset('dashboard/custom/js/datatable.js') }}"></script>
<script>
  $(document).ready(function () {
    const url = '{{ route($route.'index') }}';

    dataSourceServerSide.init('#dataTable', {
      url: url,
      column: [
        {data: 'name'},
        {data: 'reg_number'},
        {data: 'gender'},
        {data: 'email'},
        {
          data: 'subject_id',
          render: function(data, type, full){
            return full.subject ? full.subject.name : '-'
        }},
        {data: 'id', render: function(data, type, full, meta) {
          let action = `<a class="dropdown-item" href="${url}/${data}"><i class="la la-eye"></i> Detail</a>`;

          @can($permission.'edit')
          action += `<a class="dropdown-item" href="${url}/${data}/edit"><i class="la la-edit"></i> Edit</a>`
          @endcan

          @can($permission.'delete')
          action += `<form action="${url}/${data}" method="post" class="form-inline">@method('DELETE') @csrf<button class="dropdown-item delete-from-table"><i class="la la-trash"></i> Delete</button></form>`
          @endcan

          let btn = `<span class="dropdown">
            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
              <i class="la la-ellipsis-h"></i>
            </a>
            <div class="dropdown-menu">${action}</div>
          </span>`
          return btn;
        }},
      ]
    });
  });
</script>

@endsection