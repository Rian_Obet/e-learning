@extends('layouts.dashboard.app')

@section('content')
<div class="row">
  <div class="col-sm-12 col-md-6">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Detail {{ $title }}</h4>
        <a class="heading-elements-toggle"><i class="ft-align-justify font-medium-3"></i></a>
        <div class="heading-elements">
          <ul class="list-inline mb-0">
            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="card-content collapse show">
        <div class="card-body">
          <div class="row form-group">
            <div class="col-3">
              <label>Name</label>
            </div>
            <div class="col-9">
              <input type="text" value="{{ $data->name }}" disabled class="form-control">
            </div>
          </div>
          <div class="row form-group">
            <div class="col-3">
              <label>Nik</label>
            </div>
            <div class="col-9">
              <input type="text" value="{{ $data->reg_number }}" disabled class="form-control">
            </div>
          </div>
          <div class="row form-group">
            <div class="col-3">
              <label>Jenis Kelamin</label>
            </div>
            <div class="col-3">
              <input type="text" value="{{ $data->gender }}" disabled class="form-control">
            </div>
          </div>
          <div class="row form-group">
            <div class="col-3">
              <label>Mata Pelajaran</label>
            </div>
            <div class="col-3">
              <input type="text" value="{{ $data->subject->name }}" disabled class="form-control">
            </div>
          </div>
          <div class="row form-group">
            <div class="col-3">
              <label>Email</label>
            </div>
            <div class="col-9">
              <input type="text" value="{{ $data->email }}" disabled class="form-control">
            </div>
          </div>
          <div class="row form-group">
            <div class="col-3">
              <label>Image</label>
            </div>
            <div class="col-9">
              {{-- <img src="{{ $data->img_url() }}" alt="" class="w-100"> --}}
            </div>
          </div>
          <div class="text-right">
            <a href="{{ route($route.'index') }}" class="btn btn-outline-primary ml-1"> <i class="ft-arrow-left"></i> Back</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection