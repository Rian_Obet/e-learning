@extends('layouts.dashboard_student.app')

@section('content')
<div class="row">
  <div class="col-md-8 col-md-6">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Create {{ $title }}</h4>
        <a class="heading-elements-toggle"><i class="ft-align-justify font-medium-3"></i></a>
        <div class="heading-elements">
          <ul class="list-inline mb-0">
            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="card-content collapse show">
        <div class="card-body">
          <form action="{{ route($route.'store') }}" method="post">
            @csrf
            <div class="form-body">
              @include($view.'field')
            </div>
            <div class="form-actions">
              <div class="text-right">
                <button type="reset" class="btn btn-outline-warning"> <i class="ft-refresh-cw"></i> Reset</button>
                <button type="submit" class="btn btn-outline-primary ml-1"> <i class="ft-check"></i> Save</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(function() {
      $('.datetimepicker').datetimepicker({
        format:'Y-m-d H:i',
      });
  });
</script>
@endsection