@extends('layouts.dashboard_teacher.app')

@section('content')
<div class="row">
    <div class="col-sm-12 col-md-6">
        {{-- start card --}}
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Profile {{ $title }}</h4>
          <a class="heading-elements-toggle"><i class="ft-align-justify font-medium-3"></i></a>
          <div class="heading-elements">
            <ul class="list-inline mb-0"
              <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
              <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
            </ul>
          </div>
        </div>
        {{-- start card --}}
        <div class="card-content collapse show">
          <div class="card-body font-weight-bold">
            <div class="row form-group">
              <div class="col-3">
                <label>Nama</label>
              </div>
              <span>
                <div class="col-2">
                    :
                </div>
              </span>
              <div class="col-6">
                {{ $data->name }}
              </div>
            </div>
            <div class="row form-group">
              <div class="col-3">
                <label>NIK</label>
              </div>
              <span>
                <div class="col-2">
                    :
                </div>
              </span>
              <div class="col-6">
                {{ $data->reg_number }}
              </div>
            </div>
            <div class="row form-group">
              <div class="col-3">
                <label>Jenis Kelamin</label>
              </div>
              <span>
                <div class="col-2">
                    :
                </div>
              </span>
              <div class="col-3">
                {{ $data->gender == 'p' ? 'Perempuan' : 'Laki Laki'  }}
              </div>
            </div>
            <div class="row form-group">
              <div class="col-3">
                <label>Kelas</label>
              </div>
              <span>
                <div class="col-2">
                    :
                </div>
              </span>
              <div class="col-3">
                {{ $data->subject->name }}
              </div>
            </div>
            <div class="row form-group">
              <div class="col-3">
                <label>Email</label>
              </div>
              <span>
                <div class="col-2">
                    :
                </div>
              </span>
              <div class="col-6">
                {{ $data->email }}
              </div>
            </div>
          </div>
        </div>
        {{-- end card --}}
      </div>
      {{-- end card --}}
    </div>
    {{-- start gambar --}}
    <div class="col-sm-12 col-md-6">
        {{-- isi gambar --}}
        <div class="card">
            <div class="card-header">
              <h4 class="card-title">Photo {{ $title }}</h4>
              <a class="heading-elements-toggle"><i class="ft-align-justify font-medium-3"></i></a>
              <div class="heading-elements">
                <ul class="list-inline mb-0"
                  <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                  <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
              </div>
            </div>
            {{-- start card --}}
            <div class="card-content collapse show">
              <div class="card-body font-weight-bold">
                <div class="row form-group">
                  <div class="col-6">
                    <img src="{{ asset('storage/teacher/'.$data['image']) }}" alt="" class="w-100">
                    {{-- <img style="height:40%;" src="{{ asset('teacher/'.$data['image']) }}"> --}}
                  </div>
                </div>
              </div>
            </div>
            {{-- end card --}}
          </div>
    </div>
    {{-- end gambar --}}
  </div>
@endsection
