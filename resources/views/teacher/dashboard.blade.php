@extends('layouts.dashboard_teacher.app')

@section('content')
    <div class="row">
    <div class="col-xl-12 col-lg-12 col-12">
        <div class="card pull-up">
          <div class="card-content">
            <div class="card-body">
              <div class="media d-flex">
                <div class="media-body text-left">
                    <h3 class="info">{{ $examTotal->count() }}</h3>
                  <h6>Jumlah Ujian</h6>
                </div>
                <div>
                  <i class="icon-heart loaded info font-large-2 float-right"></i>
                </div>
              </div>
              <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                <div class="progress-bar bg-gradient-x-info" role="progressbar" style="width: 80%"
                aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
  <div class="col-xl-3 col-lg-6 col-12">
    <div class="card pull-up">
      <div class="card-content">
        <div class="card-body">
          <div class="media d-flex">
            <div class="media-body text-left">
                <h3 class="info">{{ $examNow->count() }}</h3>
              <h6>Total Ujian yang sedang berlangsung</h6>
            </div>
            <div>
              <i class="icon-heart loaded info font-large-2 float-right"></i>
            </div>
          </div>
          <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
            <div class="progress-bar bg-gradient-x-info" role="progressbar" style="width: 80%"
            aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-lg-6 col-12">
    <div class="card pull-up">
      <div class="card-content">
        <div class="card-body">
          <div class="media d-flex">
            <div class="media-body text-left">
                <h3 class="warning">{{ $examLate->count() }}</h3>
              <h6>Ujian yang telah selesai</h6>
            </div>
            <div>
              <i class="icon-heart warning font-large-2 float-right"></i>
            </div>
          </div>
          <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
            <div class="progress-bar bg-gradient-x-warning" role="progressbar" style="width: 65%"
            aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-lg-6 col-12">
    <div class="card pull-up">
      <div class="card-content">
        <div class="card-body">
          <div class="media d-flex">
            <div class="media-body text-left">
              <h3 class="success">146</h3>
              <h6>New Customers</h6>
            </div>
            <div>
              <i class="icon-heart success font-large-2 float-right"></i>
            </div>
          </div>
          <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
            <div class="progress-bar bg-gradient-x-success" role="progressbar" style="width: 75%"
            aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-lg-6 col-12">
    <div class="card pull-up">
      <div class="card-content">
        <div class="card-body">
          <div class="media d-flex">
            <div class="media-body text-left">
              <h3 class="danger">99.89 %</h3>
              <h6>Customer Satisfaction</h6>
            </div>
            <div>
              <i class="icon-heart danger font-large-2 float-right"></i>
            </div>
          </div>
          <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
            <div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: 85%"
            aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
