@php
  $auth_user = Auth::guard('teacher')->user();
@endphp

<div class="row">
  <div class="form-group col-md-12">
    <label>Nama Paket Soal</label>
    <input type="text" name="name" class="form-control" placeholder="nama paket soal" autocomplete="off" value="{{ old('name', (isset($isEdit) ? $data->name : '')) }}" required>
  </div>
    <div class="form-group col-md-12">
      <label for="subject_id">Mata Pelajaran</label>
      <select name="subject_id" class="form-control" >
        <option value="{{ $auth_user->subject_id }}" {{ (isset($isEdit) && $data->subject_id == $auth_user->subject->id) ? 'selected' : '' }}>{{ $auth_user->subject->name }}</option>
      </select>
      <small class="help-block"></small>
    </div>
</div>  
