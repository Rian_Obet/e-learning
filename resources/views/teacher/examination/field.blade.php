@php
  $auth_user = Auth::guard('teacher')->user();
@endphp

<div class="row">
  <div class="form-group col-md-12">
    <label for="teacher_id">Guru</label>
    <select name="teacher_id" class="form-control">
        <option value="{{ $auth_user->id }}">{{ $auth_user->name }}</option>
    </select>
    <small class="help-block"></small>
  </div>
    <div class="form-group col-md-12">
      <label for="subject_id">Mata Pelajaran</label>
      <select name="subject_id" class="form-control" >
        <option value="{{ $auth_user->subject_id }}" {{ (isset($isEdit) && $data->subject_id == $auth_user->subject->id) ? 'selected' : '' }}>{{ $auth_user->subject->name }}</option>
      </select>
      <small class="help-block"></small>
    </div>
  <div class="form-group col-md-12">
    <label>Nama Ujian</label>
    <input type="text" name="name" class="form-control" placeholder="nama ujian" autocomplete="off" value="{{ old('name', (isset($isEdit) ? $data->name : '')) }}" required>
  </div>
  <div class="form-group col-md-12">
    <label>Jumlah Soal</label>
    <input type="number" name="question_total" class="form-control" placeholder="jumlah soal" autocomplete="off" value="{{ old('question_total', (isset($isEdit) ? $data->question_total : '')) }}" required>
  </div>
  <div class="form-group col-md-12">
    <label>Tanggal Mulai</label>
    <input type="text" name="start_date" class="datetimepicker form-control" id="" placeholder="tanggal mulai" autocomplete="off" value="{{ old('start_date', (isset($isEdit) ? $data->start_date : '')) }}" required>
  </div>
  <div class="form-group col-md-12">
    <label>Tanggal Selesai</label>
    <input type="text" name="late" class="datetimepicker form-control" id="" placeholder="tanggal selesai" autocomplete="off" value="{{ old('late', (isset($isEdit) ? $data->late : '')) }}" required>
  </div>
  <div class="form-group col-md-12">
    <label>Waktu</label>
    <input type="number" name="time" class="form-control" placeholder="waktu ujian" autocomplete="off" value="{{ old('name', (isset($isEdit) ? $data->time : '')) }}" required>
  </div>
  <div class="form-group col-md-12">
    <label for="type">Acak Soal</label>
    <select name="type" class="form-control">
        <option value="" disabled selected>--- Pilih ---</option>
        @foreach ($types as $item)
        <option value="{{ $item }}" {{ (isset($isEdit) && $data->type == $item) ? 'selected' : '' }}>{{ $item }}</option>
        @endforeach
    </select>
    <small class="help-block"></small>
  </div>
  
</div>  
