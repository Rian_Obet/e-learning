<div class="row">
  <div class="form-group col-md-12">
    <label for="teacher_id">Guru</label>
    <select name="teacher_id" class="form-control">
      {{-- <option value="" disabled selected>--- Pilih ---</option> --}}
            {{-- @if (count($teachers) > 0) --}}
            @foreach ($teachers as $r)
            <option value="{{ $r->id }}" {{ (isset($isEdit) && $data->teacher_id == $r->id) ? 'selected' : '' }}>{{ $r->name }}</option>
            @endforeach
                {{-- @else
                <option value="" selected disabled>Tidak ada data</option>
                @endif --}}
    </select>
    <small class="help-block"></small>
  </div>
    <div class="form-group col-md-12">
      <label for="subject_id">Mata Pelajaran</label>
      <select name="subject_id" class="form-control">
        {{-- <option value="" disabled selected>--- Pilih ---</option> --}}
        @if (count($subjects) > 0)
        @foreach ($subjects as $r)
        <option value="{{ $r->id }}" {{ (isset($isEdit) && $data->subject_id == $r->id) ? 'selected' : '' }}>{{ $r->name }}</option>
        @endforeach
        @else
        <option value="" selected disabled>Tidak ada data</option>
        @endif
      </select>
      <small class="help-block"></small>
    </div>
  <div class="form-group col-md-12">
    <label>Soal</label>
    <textarea name="question" class="form-control" id="ckeditors" placeholder="soal" autocomplete="off" value="{{ old('question', (isset($isEdit) ? $data->question : '')) }}" cols="30" rows="10" required></textarea>
    {{-- <input type="text" name="question" class="form-control" id="ckeditors" placeholder="soal" autocomplete="off" value="{{ old('question', (isset($isEdit) ? $data->question : '')) }}" required> --}}
  </div>
  <div class="form-group col-md-12">
    <label>Jawaban A</label>
    <textarea name="select1" id="ckeditors1" class="form-control" placeholder="jumlah soal" autocomplete="off" value="{{ old('select1', (isset($isEdit) ? $data->select1 : '')) }}" required cols="30" rows="10"></textarea>
    {{-- <input type="text" name="select1" class="form-control" id="ckeditors1" placeholder="jumlah soal" autocomplete="off" value="{{ old('select1', (isset($isEdit) ? $data->select1 : '')) }}" required> --}}
  </div>
  <div class="form-group col-md-12">
    <label>Jawaban B</label>
    <textarea name="select2" id="ckeditors2" class="form-control" placeholder="jumlah soal" autocomplete="off" value="{{ old('select2', (isset($isEdit) ? $data->select2 : '')) }}" required cols="30" rows="10"></textarea>
    {{-- <input type="text" name="select2" class="form-control" id="ckeditors2" placeholder="tanggal mulai" autocomplete="off" value="{{ old('select2', (isset($isEdit) ? $data->select2 : '')) }}" required> --}}
  </div>
  <div class="form-group col-md-12">
    <label>Jawaban C</label>
    <textarea name="select3" id="ckeditors3" class="form-control" placeholder="jumlah soal" autocomplete="off" value="{{ old('select3', (isset($isEdit) ? $data->select3 : '')) }}" required cols="30" rows="10"></textarea>
    {{-- <input type="text" name="select3" class="form-control" id="ckeditors3" placeholder="tanggal selesai" autocomplete="off" value="{{ old('select3', (isset($isEdit) ? $data->select3 : '')) }}" required> --}}
  </div>
  <div class="form-group col-md-12">
    <label>Jawaban D</label>
    <textarea name="select4" id="ckeditors4" class="form-control" placeholder="jumlah soal" autocomplete="off" value="{{ old('select4', (isset($isEdit) ? $data->select4 : '')) }}" required cols="30" rows="10"></textarea>
    {{-- <input type="text" name="select4" class="form-control" id="ckeditors4" placeholder="waktu ujian" autocomplete="off" value="{{ old('select4', (isset($isEdit) ? $data->select4 : '')) }}" required> --}}
  </div>
  <div class="form-group col-md-12">
    <label>Jawaban E</label>
    <textarea name="select5" id="ckeditors5" class="form-control" placeholder="jumlah soal" autocomplete="off" value="{{ old('select5', (isset($isEdit) ? $data->select5 : '')) }}" required cols="30" rows="10"></textarea>
    {{-- <input type="text" name="select5" class="form-control" id="ckeditors5" placeholder="tanggal selesai" autocomplete="off" value="{{ old('select5', (isset($isEdit) ? $data->select5 : '')) }}" required> --}}
  </div>
  <div class="form-group col-md-12">
    <label for="">Kunci Jawaban</label>
    <select name="answer" class="form-control">
      @foreach ($abjad as $item)
      <option value="{{ $item }}" {{ (isset($isEdit) && $data->answer == $item) ? 'selected' : '' }}>{{ $item }}</option>
      @endforeach
    </select>
  </div>
  <div class="form-group col-md-12">
    <label for="">Bobot Soal</label>
    <input type="number" name="weight" id="" class="form-control" placeholder="bobot soal" autocomplete="off" value="{{ old('weight', (isset($isEdit) ? $data->weight : '')) }}" required>
  </div>

</div>
