@extends('layouts.dashboard_teacher.app')

@section('content')
<div class="row">
  <div class="col-md-8 col-md-6">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Create {{ $title }}</h4>
        <a class="heading-elements-toggle"><i class="ft-align-justify font-medium-3"></i></a>
        <div class="heading-elements">
          <ul class="list-inline mb-0">
            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="card-content collapse show">
        <div class="card-body">
          <form action="{{ route($route.'store') }}" method="post">
            @csrf
            <div class="form-body">
              @include($view.'field')
            </div>
            <div class="form-actions">
              <div class="text-right">
                <button type="reset" class="btn btn-outline-warning"> <i class="ft-refresh-cw"></i> Reset</button>
                <button type="submit" class="btn btn-outline-primary ml-1"> <i class="ft-check"></i> Save</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
    CKEDITOR.replace( 'ckeditors', {
    filebrowserUploadUrl: "{{route('teacher.post.image', ['_token' => csrf_token() ])}}",
    filebrowserUploadMethod: 'form'
    });
    CKEDITOR.replace( 'ckeditors' );
    CKEDITOR.replace( 'ckeditors1' );
    CKEDITOR.replace( 'ckeditors2' );
    CKEDITOR.replace( 'ckeditors3' );
    CKEDITOR.replace( 'ckeditors4' );
    CKEDITOR.replace( 'ckeditors5' );
    CKEDITOR.on('instanceLoaded', function(e) {e.editor.resize(920, 200)} );
  </script>
@endsection
