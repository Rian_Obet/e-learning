# Base Project 2.0

## Tentang
Base ini digunakan untuk kepentingan freelance atau kantor, ambeh teu pusing deui nyieun ti awal. Eta aja sih

## Keterangan Base Project 
- Untuk dokumentasi php nya bisa pake [Dokumentasi laravel](https://laravel.com/docs) 
- Template dashboard pake modern admin, dokumentasi style na ada di google, poho link na, kontak si cuy we ajang template na kumaha

## Custom command 
- `php artisan make:trait NamaTrait` <br> 
  Untuk membuat trait, filenya disimpen di folder `app/Traits/`
- `php artisan make:helper NamaHelper` <br> 
  Untuk membuat file helper , filenya disimpen di folder `app/Helpers/`, Ambeh si helper na bisa langsung di panggil di class lain, kudu di daftarkeun di `config/app.php`, simpen di alias paling bawah, 
  ``` php
  'NamaHelper' => App\Helpers\NamaHelper::class,
  ```
- `php artisan make:model NamaModel` <br>
  Ada sedikit perubahan pada stub model, pas membuat model, file nya otomatis pake softdelete sama pake trait updater, dan perubahan folder yang awal nya di `app/` jadi di `app/Models/`. Perubahan model menjadi seperti ini : 
  ``` php
  <?php

  namespace App\Models;

  use Illuminate\Database\Eloquent\Model;
  use Illuminate\Database\Eloquent\SoftDeletes;

  use App\Traits\Updater;

  class NamaModel extends Model{
      use SoftDeletes, Updater;
      
  }

  ```


## Cara instalasi
- Pastikan udah ada webserver nya (apache, nginx, dll)
- Udah install php, sama composer nya 
- Jalan kan `composer install`
- Buat database dengan nama apa aja, sesuai project
- Buat env nya, buat file `.env`, isi nya copas dari file `.env.example`
- konfigurasi database nya 
  ```
  DB_CONNECTION=mysql
  DB_HOST=127.0.0.1
  DB_PORT=3306
  DB_DATABASE=nama_database
  DB_USERNAME=root
  DB_PASSWORD=
  ```
- Jalanin `php artisan key:generate` di terminal / cmd nya 
- `php artisan migrate --seed` untuk data default si aplikasi nya 
- Untuk akses ke halaman admin ke url `http://{hostname}/admin`
- user default nya 
  <table border="1">
    <tr>
      <th>No</th>
      <th>Email</th>
      <th>Password</th>
    </tr>
    <tr>
      <td>1</td>
      <td>admin@gmail.com</td>
      <td>admin123</td>
    </tr>
  </table>
- jalankan `php artisan storage:link`
- Selamat mencoba