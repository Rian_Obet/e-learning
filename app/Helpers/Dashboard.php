<?php

namespace App\Helpers;

use Request;
use Auth, Str;

use Spatie\Activitylog\Models\Activity;

class Dashboard{
  public static function month($arr = false){
    $month = [
      1 => 'Januari',
      'Febuari',
      'Maret',
      'April',
      'Mei',
      'Juni',
      'Juli',
      'Agustus',
      'September',
      'Oktober',
      'November',
      'Desember',
    ];
    if(!$arr){
      $newMonth = [];
      foreach($month as $i => $v){
        $num = str_pad($i, 2, '0', STR_PAD_LEFT);
        $newMonth[$num] = $v;
      }
      $month = $newMonth;
    }
    return $month;
  }

  public static function indonesiaDateFormat($date, $time = false){
    $date1   = date('Y-m-d', strtotime($date));
    $ex_date = explode('-', $date1);
    $month   = self::month();

    if($time){
      $time   = date('H:i:s', strtotime($date));
      return $ex_date[2].' '.$month[$ex_date[1]].' '.$ex_date[0].' '.$time;
    }
    return $ex_date[2].' '.$month[$ex_date[1]].' '.$ex_date[0];
  }

  public static function activityLog($id, $model = 'App\Models\User'){
    $log = Activity::where([
      'causer_id' => $id,
      'causer_type' => $model
    ])->limit(20)->orderBy('created_at', 'desc')->get();
    return $log;
  }

  public static function name_user(){
    $auth = Auth::user();
    $name = '';
    if($auth) $name = $auth->name;
    return $name;
  }

  public static function diffForhumans($time){
    return \Carbon\Carbon::parse($time)->diffForhumans();
  }

  public static function religionArray(){
    $arr = [
      1 => 'Islam',
      'Kristen',
      'Hindu',
      'Budha',
      'Khonghucu',
      'Katolik',
      'Yahudi',
    ];
    return $arr;
  }

  public static function curl_get($url){
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL            => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING       => "",
      CURLOPT_MAXREDIRS      => 10,
      CURLOPT_TIMEOUT        => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST  => "GET",
    ));
    $res = curl_exec($curl);
    curl_close($curl);

    return $res;
  }

  public static function checkUrl($str){
    if(substr($str, 0, 8) == 'https://' || substr($str, 0, 7) == 'http://'){
      return true;
    }
    return false;
  }

  public static function linkSpace($file){
    return 'https://thrd.sgp1.digitaloceanspaces.com'.($file ? '/'.$file : '');
  }

  public static function deleteFile($path_file, $disk = null){
    $check = Storage::disk($disk)->exists($path_file);
    if($check) Storage::disk($disk)->delete($path_file);
    return true;
  }
}