<?php

namespace App\Traits;

use Dashboard;

trait Updater{
  protected static function boot(){
    parent::boot();
    static::creating(function ($model) {
      $name = Dashboard::name_user();
      $model->created_by = $name;
    });

    static::updating(function ($model) {
      $name = Dashboard::name_user();
      $model->updated_by = $name;
    });

    static::deleting(function ($model) {
      $name = Dashboard::name_user();
      $model->deleted_by = $name;
      $model->save();
    });
  }
}
