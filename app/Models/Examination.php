<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Updater;

class Examination extends Model{
    use SoftDeletes, Updater;
    
    // protected $table = 'examinations';

    protected $fillable = [
        'teacher_id',
        'subject_id',
        'name',
        'question_total',
        'time',
        'type',
        'start_date',
        'late',
    ];

    public function teachers()
    {
        return $this->belongsTo(Teachers::class, 'teacher_id');
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    

    public static function type()
    {
        return [
            'acak',
            'urut'
        ];
    }
}   
