<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Updater;

class Classes extends Model{
    use SoftDeletes, Updater;
    
    protected $table = ('classes');

    protected $fillable = [
        'name'
    ];

    public function student()
    {
        return $this->hasMany(Student::class);
    }
}
