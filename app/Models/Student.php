<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Spatie\Permission\Traits\HasRoles;

use App\Traits\Updater;

use Storage;


class Student extends Authenticatable
{
    use SoftDeletes, Updater;
    
    protected $fillable = [
        'name',
        'nis',
        'gender',
        'email',
        'password',
        'class_id',
        'image'
    ];


    public function class()
    {
        return $this->belongsTo(Classes::class);
    }

    protected $disk = null;
    protected $path = 'public/student/';

    public function img_url($size = null){
        $url  = asset('dashboard/images/default.jpg'); // default 
        $size = (!is_null($size) ? $size.'/' : '');
        $path = $this->path.$size.$this->image;

        if($this->image && Storage::disk($this->disk)->exists($path)) $url = Storage::disk($this->disk)->url($path);
        return $url;
    }
    
}
