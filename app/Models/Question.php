<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Updater;

class Question extends Model{
    use SoftDeletes, Updater;

    protected $fillable = [
        'teacher_id',
        'subject_id',
        'examination_id',
        'weight',
        'file',
        'question',
        'select1',
        'select2',
        'select3',
        'select4',
        'select5',
        'answer',
    ];

    public function teacher()
    {
        return $this->belongsTo('App\Models\Teachers');
    }

    public function subject()
    {
        return $this->belongsTo('App\Models\Subject');
    }

    public function examination()
    {
        return $this->belongsTo('App\Models\Examination');
    }

    public static function abjad()
    {
        return  [
            'A',
            'B',
            'C',
            'D',
            'E',
        ];
    }
}
