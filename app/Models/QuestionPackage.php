<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Updater;

class QuestionPackage extends Model{
    use SoftDeletes, Updater;

    protected $table = ('question_packages');

    protected $fillable = [
        'name',
        'subject_id',
    ];

    public function subject()
    {
        return $this->belongsTo('App\Models\Subject');
    }

    public function question()
    {
        return $this->belongsTo('App\Models\Question');
    }

}
