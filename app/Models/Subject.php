<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Updater;

class Subject extends Model{
    use SoftDeletes, Updater;
    
    protected $fillable = [
        'name'
    ];
}
