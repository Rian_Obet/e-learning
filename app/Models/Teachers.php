<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Spatie\Permission\Traits\HasRoles;

use App\Traits\Updater;

use Storage;


class Teachers extends Authenticatable
{
    use SoftDeletes, Updater;

    protected $fillable = [
        'name',
        'reg_number',
        'gender',
        'email',
        'password',
        'subject_id',
        'image'
    ];


    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    protected $disk = null;
    protected $path = 'public/student/';

    public function img_url($size = null){
        $url  = asset('dashboard/images/default.jpg'); // default
        $size = (!is_null($size) ? $size.'/' : '');
        $path = $this->path.$size.$this->image;

        if($this->image && Storage::disk($this->disk)->exists($path)) $url = Storage::disk($this->disk)->url($path);
        return $url;
    }

}
