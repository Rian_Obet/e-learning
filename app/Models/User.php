<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Spatie\Permission\Traits\HasRoles;

use App\Traits\Updater;

use Storage;

class User extends Authenticatable
{
    use HasRoles, SoftDeletes, Updater;

    protected $fillable = [
        'name',
        'email',
        'password',
        'image',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $disk = null;
    protected $path = 'public/users/';

    public function img_url($size = null){
        $url  = asset('dashboard/images/default.jpg'); // default 
        $size = (!is_null($size) ? $size.'/' : '');
        $path = $this->path.$size.$this->image;

        if($this->image && Storage::disk($this->disk)->exists($path)) $url = Storage::disk($this->disk)->url($path);
        return $url;
    }
}
