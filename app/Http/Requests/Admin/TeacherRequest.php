<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

use Route;

class TeacherRequest extends FormRequest
{
    public function authorize(){
        return true;
    }

    public function rules(){
        $rules = [
            'name'     => 'required|string|max:191',
            'reg_number'    => 'required|string|max:191',
            'gender'    => 'required',
            'email'    => 'required|email|max:191',
            'password' => 'required',
            'subject_id' => 'required',
            'image'    => 'nullable|file|image'
        ];

        // if(Route::is('admin.user.update') || Route::is('admin.profile.store')){
        //     $rules['password'] = 'confirmed';
        //     unset($rules['email']);
        // }

        return $rules;
    }
}
