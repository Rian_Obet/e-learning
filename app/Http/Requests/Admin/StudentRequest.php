<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StudentRequest extends FormRequest
{
    public function authorize(){
        return true;
    }

    public function rules(){
        $rules = [
            'name'     => 'required|string|max:191',
            'nis'    => 'required|string|max:191',
            'gender'    => 'required',
            'email'    => 'required|email|max:191',
            'password' => 'required',
            'class_id' => 'required',
            'image'    => 'nullable|file|image'
        ];

        // if(Route::is('admin.user.update') || Route::is('admin.profile.store')){
        //     $rules['password'] = 'confirmed';
        //     unset($rules['email']);
        // }

        return $rules;
    }
}
