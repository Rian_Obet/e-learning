<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

use Route;

class UserRequest extends FormRequest
{
    public function authorize(){
        return true;
    }

    public function rules(){
        $rules = [
            'name'     => 'required|string|max:191',
            'email'    => 'required|email|max:191',
            'password' => 'required|confirmed',
            'image'    => 'nullable|file|image'
        ];

        if(Route::is('admin.user.update') || Route::is('admin.profile.store')){
            $rules['password'] = 'confirmed';
            unset($rules['email']);
        }

        return $rules;
    }
}
