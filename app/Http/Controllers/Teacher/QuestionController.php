<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use App\Models\Question;
use Illuminate\Http\Request;

use App\Models\Teachers;
use App\Models\Subject;

use Illuminate\Support\Facades\Auth;
use Datatables;
use View, DB;
use UploadImg;

class QuestionController extends Controller
{
    protected $model;
    protected $title = 'Question';
    protected $view  = 'teacher.question.';
    protected $route = 'teacher.question.';
    protected $permission = 'Question ';
    protected $path = 'public/question/';
    protected $icon = 'ft-book';

    public function __construct(Question $model){
        $this->model = $model;

        View::share('route', $this->route);
        View::share('title', $this->title);
        View::share('view', $this->view);

    }

    protected function user(){
        return auth()->user();
    }

    public function index(Request $req){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['List '.$this->title, null]
        ]);

        if($req->ajax()) {
            $user = Auth::user()->id;
            $data = $this->model->with('teacher', 'subject')->where('teacher_id', '=', $user);
            return Datatables::of($data)->make(true);
        };


        return view($this->view.'index');
    }

    public function create(){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['Create '.$this->title, null]
        ]);

        $user = Auth::user()->id;
        $userS = Auth::user()->subject->id;

        $this->data['subjects'] = Subject::where('id', '=', $userS)->get();
        $this->data['teachers'] = Teachers::where('id', '=', $user)->get();
        // $this->data['data'] = Examination::orderBy('name', 'ASC');
        $this->data['abjad'] = Question::abjad();

        // dd($this->data['types']);

        return view($this->view.'create', $this->data);
    }

    public function store(Request $req){
        DB::beginTransaction();
        try {
            $input = $req->all();
                $data = $this->model->create($input);

            if($data){
                activity()
                    ->causedBy($this->user())
                    ->withProperties([
                        'icon'  => $this->icon,
                        'title' => $this->title,
                        'type'  => 'create'
                    ])->log('Create '.$this->title);

                DB::commit();
                alert()->success('Berhasil', 'Data telah berhasil disimpan');
                return redirect()->route($this->route.'index');
            }

            alert()->error('Gagal', 'Data telah gagal disimpan');
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            alert()->error('Gagal', $e->getMessage());
            return redirect()->back();
        }
    }
}
