<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Models\Examination;
use App\Models\Subject;
use App\Models\Teachers;
use Spatie\Permission\Models\Role;

use Datatables;
use Illuminate\Support\Facades\Auth;
use View, DB;
use UploadImg;

class ExaminationController extends Controller
{
    protected $model;
    protected $title = 'Examination';
    protected $view  = 'teacher.examination.';
    protected $route = 'teacher.examination.';
    protected $path = 'public/users/';
    protected $icon = 'ft-book';

    public function __construct(Examination $model){
        $this->model = $model;

        View::share('route', $this->route);
        View::share('title', $this->title);
        View::share('view', $this->view);
        View::share('teachers', Teachers::all());
        View::share('subject', Subject::all());

    }

    protected function user(){
        return auth()->user();
    }

    public function index(Request $req){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['List '.$this->title, null]
        ]);

        if($req->ajax()) {
            $user = Auth::user()->id;
            $data = $this->model->with('teachers', 'subject')->where('teacher_id', '=', $user);
            return Datatables::of($data)->make(true);
        };


        return view($this->view.'index');
    }

    public function create(){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['Create '.$this->title, null]
        ]);

        $this->data['subjects'] = Subject::orderBy('name', 'ASC')->get();
        $this->data['teachers'] = Teachers::orderBy('name', 'ASC')->get();
        $this->data['types'] = Examination::type();
        // $this->data['data'] = Examination::orderBy('name', 'ASC');

        // dd($this->data['types']);

        return view($this->view.'create', $this->data);
    }

    public function store(Request $req){
        DB::beginTransaction();
        try {
            $input = $req->all();
            // dd($input);
                $data = $this->model->create($input);
            if($data){
                activity()
                    ->causedBy($this->user())
                    ->withProperties([
                        'icon'  => $this->icon,
                        'title' => $this->title,
                        'type'  => 'create'
                    ])->log('Create '.$this->title);

                DB::commit();
                alert()->success('Berhasil', 'Data telah berhasil disimpan');
                return redirect()->route($this->route.'index');
            }

            alert()->error('Gagal', 'Data telah gagal disimpan');
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            alert()->error('Gagal', $e->getMessage());
            return redirect()->back();
        }
    }

    public function edit($id){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['Edit '.$this->title, null]
        ]);
        $data = $this->model->findOrFail($id);
        $subjects = Subject::orderBy('name', 'ASC')->get();
        $teachers = Teachers::orderBy('name', 'ASC')->get();
        $types = Examination::type();

        return view($this->view.'edit', compact('data', 'id', 'subjects', 'teachers', 'types'));
    }

    public function update(Request $req, $id){
        DB::beginTransaction();
        try {
            $input = $req->all();

            $data = $this->model->findOrFail($id);
            if($data->update($input)){
                activity()
                    ->causedBy($this->user())
                    ->performedOn($data)
                    ->withProperties([
                        'icon'  => $this->icon,
                        'title' => $this->title,
                        'type'  => 'edit'
                    ])->log('Edit '.$this->title);

                DB::commit();
                alert()->success('Berhasil', 'Data telah berhasil disimpan');
                return redirect()->route($this->route.'index');
            }

            alert()->error('Gagal', 'Data telah gagal disimpan');
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            alert()->error('Gagal', $e->getMessage());
            return redirect()->back();
        }
    }

    public function destroy($id){
        $data = $this->model->findOrFail($id);
        if($data->delete()){
            activity()
                ->causedBy($this->user())
                ->withProperties([
                    'icon'  => $this->icon,
                    'title' => $this->title,
                    'type'  => 'delete'
                ])->log('Delete '.$this->title);

            alert()->success('Berhasil', 'Data telah berhasil dihapus');
            return redirect()->route($this->route.'index');
        }

        alert()->error('Gagal', 'Data telah gagal dihapus');
        return redirect()->back();
    }
}
