<?php

namespace App\Http\Controllers\Teacher;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Spatie\Permission\Models\Permission;

use App\Models\Subject;
use App\Models\Question;
use App\Models\QuestionPackage;

use Illuminate\Support\Facades\Auth;

use Datatables;
use UploadImg;
use View, DB;

class QuestionPackageController extends Controller
{
    protected $model;
    protected $title = 'Paket Soal';
    protected $view  = 'teacher.question_package.';
    protected $route = 'teacher.question_package.';
    protected $icon = 'ft-shield';

    public function __construct(QuestionPackage $model){
        $this->model = $model;

        View::share('route', $this->route);
        View::share('title', $this->title);
        View::share('view', $this->view);
        View::share('question', Question::all());
        View::share('subject', Subject::all());

    }

    protected function user(){
        return auth()->user();
    }

    public function index(Request $req){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['List '.$this->title, null]
        ]);

        if($req->ajax()) {
            $user = Auth::user()->subject_id;
            $data = $this->model->with('subject')->where('subject_id', '=', $user);
            return Datatables::of($data)->make(true);
        };
        
        
        return view($this->view.'index');
        
        // View::share('breadcrumbs', [
        //     [$this->title, route($this->route.'index')],
        //     ['List '.$this->title, null]
        // ]);
        
        // if($req->ajax()) {
        //     $data = $this->model->with('subject')->orderBy('name', 'asc');
        //     return Datatables::of($data)->make(true);
        // };

        // return view($this->view.'index');
        
    }

    public function create(){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['Create '.$this->title, null]
        ]);

        return view($this->view.'create');
    }

    public function store(Request $req){
        DB::beginTransaction();
        try {
            $input = $req->all();
                $data = $this->model->create($input);

            if($data){
                activity()
                    ->causedBy($this->user())
                    ->withProperties([
                        'icon'  => $this->icon,
                        'title' => $this->title,
                        'type'  => 'create'
                    ])->log('Create '.$this->title);

                DB::commit();
                alert()->success('Berhasil', 'Data telah berhasil disimpan');
                return redirect()->route($this->route.'index');
            }

            alert()->error('Gagal', 'Data telah gagal disimpan');
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            alert()->error('Gagal', $e->getMessage());
            return redirect()->back();
        }
    }

    public function show($id){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['Detail '.$this->title, null]
        ]);
        $data = $this->model->findOrFail($id);

        return view($this->view.'show', compact('data'));
    }

    public function edit($id){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['Edit '.$this->title, null]
        ]);
        $data = $this->model->findOrFail($id);

        return view($this->view.'edit', compact('data', 'id'));
    }

    public function update(Request $req, $id){
        DB::beginTransaction();
        try {
            $input = $req->only(['name']);

            $data = $this->model->findOrFail($id);
            if($data->update($input)){
                activity()
                    ->causedBy($this->user())
                    ->performedOn($data)
                    ->withProperties([
                        'icon'  => $this->icon,
                        'title' => $this->title,
                        'type'  => 'edit'
                    ])->log('Edit '.$this->title);

                DB::commit();
                alert()->success('Berhasil', 'Data telah berhasil disimpan');
                return redirect()->route($this->route.'index');
            }

            alert()->error('Gagal', 'Data telah gagal disimpan');
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            alert()->error('Gagal', $e->getMessage());
            return redirect()->back();
        }
    }

    public function configureForm($id){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['Edit '.$this->title, null]
        ]);

        $data = $this->model->findOrFail($id);
        $isPackage = $data->permissions->pluck('id')->toArray();
        $permissions = Permission::orderBy('name', 'asc')->get();

        return view($this->view.'configure', compact('id', 'data', 'permissions', 'isPermission'));
    }

    public function destroy($id){
        $data = $this->model->findOrFail($id);
        if($data->delete()){
            activity()
                ->causedBy($this->user())
                ->withProperties([
                    'icon'  => $this->icon,
                    'title' => $this->title,
                    'type'  => 'delete'
                ])->log('Delete '.$this->title);

            alert()->success('Berhasil', 'Data telah berhasil dihapus');
            return redirect()->route($this->route.'index');
        }

        alert()->error('Gagal', 'Data telah gagal dihapus');
        return redirect()->back();
    }
}
