<?php

namespace App\Http\Controllers\Teacher;
use App\Models\Teachers;
use App\Models\Student;
use App\Http\Controllers\Controller;
use App\Models\Examination;
use Illuminate\Http\Request;

use Auth, View;
use Carbon\Carbon;

class DashboardController extends Controller
{
    protected $view  = 'teacher.';
    protected $route = 'teacher.';
    protected $path = 'public/storage/teacher/';


    public function __construct(Teachers $model){
        $this->model = $model;

        View::share('route', $this->route);
        View::share('view', $this->view);

    }

    protected function guard(){
        return Auth::guard();
    }

    public function index(){

        $this->data['title'] = 'Guru';
        $user = Auth::user()->id;
        $this->data['examTotal'] = Examination::where('teacher_id', '=', $user);
        $this->data['examLate'] = Examination::where('late', '<', Carbon::today())->where('teacher_id', '=', $user);
        $this->data['examNow'] = Examination::where('late', '>', Carbon::today())->where('teacher_id', '=', $user);
        // dd($this->data['examNow']);

        return view($this->view.'dashboard', $this->data);

    }

    public function profile()
    {
        $title = 'Guru';

        $data = Auth::guard('teacher')->user();

        return view($this->view.'profile', compact('data', 'title'));
    }
}
