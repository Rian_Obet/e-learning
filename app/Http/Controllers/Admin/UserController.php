<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UserRequest;

use App\Models\User;

use Spatie\Permission\Models\Role;

use Datatables;
use View, DB;
use UploadImg;

class UserController extends Controller
{
    protected $model;
    protected $title = 'User';
    protected $view  = 'admin.user.';
    protected $route = 'admin.user.';
    protected $permission = 'User ';
    protected $path = 'public/users/';
    protected $icon = 'ft-users';

    public function __construct(User $model){
        $this->model = $model;

        View::share('route', $this->route);
        View::share('title', $this->title);
        View::share('view', $this->view);
        View::share('permission', $this->permission);

        $this->middleware('permission:'.$this->permission.'index')->only('index', 'show');
        $this->middleware('permission:'.$this->permission.'create')->only('create', 'store');
        $this->middleware('permission:'.$this->permission.'edit')->only('edit', 'update');
        $this->middleware('permission:'.$this->permission.'delete')->only('destroy');
        $this->middleware('permission:'.$this->permission.'configure')->only('configureForm', 'configure');
    }

    protected function user(){
        return auth()->user();
    }

    public function index(Request $req){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['List '.$this->title, null]
        ]);

        if($req->ajax()) {
            $data = $this->model->with('roles');
            return Datatables::of($data)->make(true);
        };

        return view($this->view.'index');
    }

    public function create(){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['Create '.$this->title, null]
        ]);

        return view($this->view.'create');
    }

    public function store(UserRequest $req){
        DB::beginTransaction();
        try {
            $input = $req->only(['name', 'email', 'password']);
            $input['password'] = bcrypt($input['password']);

            $img = $req->file('image');
            if(!is_null($img)){
                $upload = new UploadImg($img);
                $upload->setPath($this->path);
                $input['image'] = $upload->getFilename();
            }

            $data = $this->model->create($input);
            if($data){
                if(!is_null($img)) $upload->upload();

                activity()
                    ->causedBy($this->user())
                    ->performedOn($data)
                    ->withProperties([
                        'icon'  => $this->icon,
                        'title' => $this->title,
                        'type'  => 'create'
                    ])->log('Create '.$this->title);

                DB::commit();
                alert()->success('Berhasil', 'Data telah berhasil disimpan');
                return redirect()->route($this->route.'index');
            }

            alert()->error('Gagal', 'Data telah gagal disimpan');
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            alert()->error('Gagal', $e->getMessage());
            return redirect()->back();
        }
    }

    public function show($id){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['Detail '.$this->title, null]
        ]);
        $data = $this->model->findOrFail($id);

        return view($this->view.'show', compact('data'));
    }

    public function configureForm($id){
        $data = $this->model->findOrFail($id);
        $isRole = $data->roles->pluck('id')->toArray();
        $roles = Role::orderBy('name', 'asc')->get();

        return view($this->view.'configure', compact('data', 'id','roles', 'isRole'));
    }

    public function configure(Request $req, $id){
        DB::beginTransaction();
        try {
            $data = $this->model->findOrFail($id);
            $isRole = $data->roles->pluck('id')->toArray();
            $input = $req->roles;
            $del = json_decode($req->delete_roles);
            if(count($del)){
                $dataDel = [];
                foreach($del as $r){
                    $role = Role::find($r);
                    if($role) $data->removeRole($role);
                }
            }

            if($input){
                foreach($input as $r){
                    if(!in_array($r, $isRole)){
                        $role = Role::find($r);
                        if($role) $data->assignRole($role);
                    }
                }
            }

            activity()
                ->causedBy($this->user())
                ->performedOn($data)
                ->withProperties([
                    'icon'  => $this->icon,
                    'title' => $this->title,
                    'type'  => 'configure'
                ])->log('Configure '.$this->title);

            DB::commit();

            alert()->success('Berhasil', 'Perubahan berhasil disimpan');
            return redirect()->route($this->route.'index');
        } catch (\Exception $e) {
            DB::rollback();

            alert()->error('Gagal', $e->getMessage());
            return redirect()->back();
        }
    }

    public function edit($id){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['Edit '.$this->title, null]
        ]);
        $data = $this->model->findOrFail($id);

        return view($this->view.'edit', compact('data', 'id'));
    }

    public function update(UserRequest $req, $id){
        DB::beginTransaction();
        try {
            $input = $req->only(['name', 'password']);
            if($input['password']){
                $input['password'] = bcrypt($input['password']);
            }else{
                unset($input['password']);
            }

            $img = $req->file('image');
            if(!is_null($img)){
                $upload = new UploadImg($img);
                $upload->setPath($this->path);
                $input['image'] = $upload->getFilename();
            }

            $data = $this->model->findOrFail($id);
            if($data->update($input)){
                if(!is_null($img)) $upload->upload();

                activity()
                    ->causedBy($this->user())
                    ->performedOn($data)
                    ->withProperties([
                        'icon'  => $this->icon,
                        'title' => $this->title,
                        'type'  => 'edit'
                    ])->log('Edit '.$this->title);

                DB::commit();
                alert()->success('Berhasil', 'Data telah berhasil disimpan');
                return redirect()->route($this->route.'index');
            }

            alert()->error('Gagal', 'Data telah gagal disimpan');
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            alert()->error('Gagal', $e->getMessage());
            return redirect()->back();
        }
    }

    public function destroy($id){
        if($id == $this->user()->id){
            swal()->error('Gagal', 'User sedang anda gunakan');
            return redirect()->back();
        }

        $data = $this->model->findOrFail($id);
        if($data->delete()){
            activity()
                ->causedBy($this->user())
                ->withProperties([
                    'icon'  => $this->icon,
                    'title' => $this->title,
                    'type'  => 'delete'
                ])->log('Delete '.$this->title);

            alert()->success('Berhasil', 'Data telah berhasil dihapus');
            return redirect()->route($this->route.'index');
        }

        alert()->error('Gagal', 'Data telah gagal dihapus');
        return redirect()->back();
    }
}
