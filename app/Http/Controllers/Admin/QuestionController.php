<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Question;
use Illuminate\Http\Request;

use App\Models\Teachers;
use App\Models\Subject;

use Datatables;
use View, DB;
use UploadImg;

class QuestionController extends Controller
{
    protected $model;
    protected $title = 'Bank Soal';
    protected $view  = 'admin.question.';
    protected $route = 'admin.question.';
    protected $permission = 'Question ';
    protected $path = 'public/question/';
    protected $icon = 'ft-book';

    public function __construct(Question $model){
        $this->model = $model;

        View::share('route', $this->route);
        View::share('title', $this->title);
        View::share('view', $this->view);
        View::share('permission', $this->permission);

        $this->middleware('permission:'.$this->permission.'index')->only('index', 'show');
        $this->middleware('permission:'.$this->permission.'create')->only('create', 'store');
        $this->middleware('permission:'.$this->permission.'edit')->only('edit', 'update');
        $this->middleware('permission:'.$this->permission.'delete')->only('destroy');

    }

    protected function user(){
        return auth()->user();
    }

    public function index(Request $req){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['List '.$this->title, null]
        ]);

        if($req->ajax()) {
            $data = $this->model->with('teacher', 'subject')->latest()->get();
            return Datatables::of($data)->make(true);
        };


        return view($this->view.'index');
    }

    public function create(){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['Create '.$this->title, null]
        ]);

        $this->data['subjects'] = Subject::orderBy('name', 'ASC')->get();
        $this->data['teachers'] = Teachers::orderBy('name', 'ASC')->get();
        // $this->data['data'] = Examination::orderBy('name', 'ASC');
        $this->data['abjad'] = Question::abjad();

        // dd($this->data['types']);

        return view($this->view.'create', $this->data);
    }

    public function store(Request $req){
        DB::beginTransaction();
        try {
            $input = $req->all();
                $data = $this->model->create($input);

            if($data){
                activity()
                    ->causedBy($this->user())
                    ->withProperties([
                        'icon'  => $this->icon,
                        'title' => $this->title,
                        'type'  => 'create'
                    ])->log('Create '.$this->title);

                DB::commit();
                alert()->success('Berhasil', 'Data telah berhasil disimpan');
                return redirect()->route($this->route.'index');
            }

            alert()->error('Gagal', 'Data telah gagal disimpan');
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            alert()->error('Gagal', $e->getMessage());
            return redirect()->back();
        }
    }

    public function edit($id){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['Edit '.$this->title, null]
        ]);
        $data = $this->model->findOrFail($id);
        $subjects = Subject::orderBy('name', 'ASC')->get();
        $teachers = Teachers::orderBy('name', 'ASC')->get();
        $abjad = Question::abjad();

        return view($this->view.'edit', compact('data', 'id', 'subjects', 'teachers', 'abjad'));
    }

    public function update(Request $req, $id){
        DB::beginTransaction();
        try {
            $input = $req->all();

            $data = $this->model->findOrFail($id);
            if($data->update($input)){
                activity()
                    ->causedBy($this->user())
                    ->performedOn($data)
                    ->withProperties([
                        'icon'  => $this->icon,
                        'title' => $this->title,
                        'type'  => 'edit'
                    ])->log('Edit '.$this->title);

                DB::commit();
                alert()->success('Berhasil', 'Data telah berhasil disimpan');
                return redirect()->route($this->route.'index');
            }

            alert()->error('Gagal', 'Data telah gagal disimpan');
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            alert()->error('Gagal', $e->getMessage());
            return redirect()->back();
        }
    }

    public function destroy($id){
        $data = $this->model->findOrFail($id);
        if($data->delete()){
            activity()
                ->causedBy($this->user())
                ->withProperties([
                    'icon'  => $this->icon,
                    'title' => $this->title,
                    'type'  => 'delete'
                ])->log('Delete '.$this->title);

            alert()->success('Berhasil', 'Data telah berhasil dihapus');
            return redirect()->route($this->route.'index');
        }

        alert()->error('Gagal', 'Data telah gagal dihapus');
        return redirect()->back();
    }

    public function uploadImage(Request $req)
    {
        //JIKA ADA DATA YANG DIKIRIMKAN
        if ($req->hasFile('upload')) {
            $file = $req->file('upload');
            $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            //KEMUDIAN GENERATE NAMA YANG BARU KOMBINASI NAMA FILE + TIME
            $filename = $filename . '-' . time(). '.' . $file->getClientOriginalExtension();

            $file->move(public_path('uploads'), $filename);

             //KEMUDIAN KITA BUAT RESPONSE KE CKEDITOR
             $ckeditor = $req->input('CKEditorFuncNum');
            $url = asset('uploads/' . $filename);
            $msg = 'Image uploaded successfully';
            //DENGNA MENGIRIMKAN INFORMASI URL FILE DAN MESSAGE
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($ckeditor, '$url', '$msg')</script>";

            //SET HEADERNYA
            @header('Content-type: text/html; charset=utf-8');
            return $response;
        }
    }


}
