<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth, View;

class DashboardController extends Controller
{
    protected $view  = 'admin.';
    protected $route = 'admin.';

    public function __construct(){
        View::share('route', $this->route);
    }

    protected function guard(){
        return Auth::guard();
    }

    public function index(){
        $title = 'Dashboard';

        return view($this->view.'dashboard', compact('title'));
    }
}
