<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\Admin\StudentRequest;

use App\Models\Student;
use App\Models\Classes;

use Spatie\Permission\Models\Role;

use Datatables;
use View, DB;
use UploadImg;

class StudentController extends Controller
{
    protected $model;
    protected $title = 'Siswa';
    protected $view  = 'admin.student.';
    protected $route = 'admin.student.';
    protected $permission = 'Student ';
    protected $path = 'public/student/';
    protected $icon = 'ft-users';

    public function __construct(Student $model){
        $this->model = $model;

        View::share('route', $this->route);
        View::share('title', $this->title);
        View::share('view', $this->view);
        View::share('permission', $this->permission);
        View::share('class', Classes::all());

        $this->middleware('permission:'.$this->permission.'index')->only('index', 'show');
        $this->middleware('permission:'.$this->permission.'create')->only('create', 'store');
        $this->middleware('permission:'.$this->permission.'edit')->only('edit', 'update');
        $this->middleware('permission:'.$this->permission.'delete')->only('destroy');
    }

    protected function user(){
        return auth()->user();
    }

    public function index(Request $req)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['List '.$this->title, null]
        ]);

        if($req->ajax()) {
            $data = $this->model->with('class')->orderBy('name');
            return Datatables::of($data)->make(true);
        };

        return view($this->view.'index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['Create '.$this->title, null]
        ]);

        return view($this->view.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentRequest $req)
    {
        DB::beginTransaction();
        try {
            $input = $req->only(['name', 'nis', 'gender', 'email', 'password', 'class_id', 'image']);
            $input['password'] = bcrypt($input['password']);

            $img = $req->file('image');
            if(!empty($img)){
                $upload = new UploadImg($img);
                $upload->setPath($this->path);
                $input['image'] = $upload->getFilename();
            }

            $data = $this->model->create($input);
            if($data){
                if(!is_null($img)) $upload->upload();

                activity()
                    ->causedBy($this->user())
                    ->performedOn($data)
                    ->withProperties([
                        'icon'  => $this->icon,
                        'title' => $this->title,
                        'type'  => 'create'
                    ])->log('Create '.$this->title);

                DB::commit();
                alert()->success('Berhasil', 'Data telah berhasil disimpan');
                return redirect()->route($this->route.'index');
              }
                alert()->error('Gagal', 'Data telah gagal disimpan');
                return redirect()->back();
            } catch (\Exception $e) {
                DB::rollback();

                alert()->error('Gagal', $e->getMessage());
                return redirect()->back();
             }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['Detail '.$this->title, null]
        ]);
        $data = $this->model->whereId($id)->with('class')->findOrFail($id);

        return view($this->view.'show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['Edit '.$this->title, null]
        ]);
        $data = $this->model->findOrFail($id);

        return view($this->view.'edit', compact('data', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StudentRequest $req, $id)
    {
        DB::beginTransaction();
        try {
            $input = $req->only(['name', 'nis', 'gender', 'email', 'password', 'class_id', 'image']);
            if($input['password']){
                $input['password'] = bcrypt($input['password']);
            }else{
                unset($input['password']);
            }

            $img = $req->file('image');
            if(!is_null($img)){
                $upload = new UploadImg($img);
                $upload->setPath($this->path);
                $input['image'] = $upload->getFilename();
            }

            $data = $this->model->findOrFail($id);
            if($data->update($input)){
                if(!is_null($img)) $upload->upload();

                activity()
                    ->causedBy($this->user())
                    ->performedOn($data)
                    ->withProperties([
                        'icon'  => $this->icon,
                        'title' => $this->title,
                        'type'  => 'edit'
                    ])->log('Edit '.$this->title);

                DB::commit();
                alert()->success('Berhasil', 'Data telah berhasil disimpan');
                return redirect()->route($this->route.'index');
            }

            alert()->error('Gagal', 'Data telah gagal disimpan');
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            alert()->error('Gagal', $e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id == $this->user()->id){
            swal()->error('Gagal', 'User sedang anda gunakan');
            return redirect()->back();
        }

        $data = $this->model->findOrFail($id);
        if($data->delete()){
            activity()
                ->causedBy($this->user())
                ->withProperties([
                    'icon'  => $this->icon,
                    'title' => $this->title,
                    'type'  => 'delete'
                ])->log('Delete '.$this->title);

            alert()->success('Berhasil', 'Data telah berhasil dihapus');
            return redirect()->route($this->route.'index');
        }

        alert()->error('Gagal', 'Data telah gagal dihapus');
        return redirect()->back();
    }
}
