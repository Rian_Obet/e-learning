<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth, View;

class LoginController extends Controller
{
    protected $view  = 'admin.';
    protected $route = 'admin.';

    public function __construct(){
        View::share('route', $this->route);
    }
    
    protected function guard(){
        return Auth::guard();
    }

    public function showLoginForm(){
        $title = 'Login';
        return view($this->view.'login', compact('title'));
    }

    public function login(Request $req){
        $this->validation($req);
        $input = $req->only('email', 'password');
        
        if($this->guard()->attempt($input, $req->filled('remember'))){
            return redirect()->route($this->route.'index');
        }

        alert()->error('Gagal', 'Login gagal!');
        return redirect()->back();
    }

    protected function validation(Request $req){
        $req->validate([
            'email'    => 'required|email|max:50',
            'password' => 'required|string'
        ]);
    }

    public function logout(){
        $this->guard()->logout();
        return redirect()->route($this->route.'login');
    }
}
