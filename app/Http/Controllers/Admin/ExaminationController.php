<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Models\Examination;
use App\Models\Question;
use App\Models\Subject;
use App\Models\Teachers;
use Spatie\Permission\Models\Role;

use Datatables;
use View, DB;

use UploadImg;


class ExaminationController extends Controller
{
    protected $model;
    protected $title = 'Ujian';
    protected $view  = 'admin.examination.';
    protected $route = 'admin.examination.';
    protected $permission = 'Examination ';
    protected $path = 'public/users/';
    protected $icon = 'ft-book';

    public function __construct(Examination $model){
        $this->model = $model;

        View::share('route', $this->route);
        View::share('title', $this->title);
        View::share('view', $this->view);
        View::share('permission', $this->permission);
        View::share('teacher', Teachers::all());
        View::share('subject', Subject::all());

        $this->middleware('permission:'.$this->permission.'index')->only('index', 'show');
        $this->middleware('permission:'.$this->permission.'create')->only('create', 'store');
        $this->middleware('permission:'.$this->permission.'edit')->only('edit', 'update');
        $this->middleware('permission:'.$this->permission.'delete')->only('destroy');

    }

    protected function user(){
        return auth()->user();
    }

    public function index(Request $req){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['List '.$this->title, null]
        ]);

        if($req->ajax()) {
            $data = $this->model->with('teachers', 'subject')->latest()->get();
            return Datatables::of($data)->make(true);
        };


        return view($this->view.'index');
    }

    public function create(){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['Create '.$this->title, null]
        ]);

        $this->data['subjects'] = Subject::orderBy('name', 'ASC')->get();
        $this->data['teachers'] = Teachers::orderBy('name', 'ASC')->get();
        $this->data['types'] = Examination::type();
        // $this->data['data'] = Examination::orderBy('name', 'ASC');

        // dd($this->data['types']);

        return view($this->view.'create', $this->data);
    }

    public function store(Request $req){
        DB::beginTransaction();
        try {
            $input = $req->all();
            // dd($input);
                $data = $this->model->create($input);
            if($data){
                activity()
                    ->causedBy($this->user())
                    ->withProperties([
                        'icon'  => $this->icon,
                        'title' => $this->title,
                        'type'  => 'create'
                    ])->log('Create '.$this->title);

                DB::commit();
                alert()->success('Berhasil', 'Data telah berhasil disimpan');
                return redirect()->route($this->route.'index');
            }

            alert()->error('Gagal', 'Data telah gagal disimpan');
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            alert()->error('Gagal', $e->getMessage());
            return redirect()->back();
        }
    }

    public function edit($id){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['Edit '.$this->title, null]
        ]);
        $data = $this->model->findOrFail($id);
        $subjects = Subject::orderBy('name', 'ASC')->get();
        $teachers = Teachers::orderBy('name', 'ASC')->get();
        $types = Examination::type();

        return view($this->view.'edit', compact('data', 'id', 'subjects', 'teachers', 'types'));
    }

    public function update(Request $req, $id){
        DB::beginTransaction();
        try {
            $input = $req->all();

            $data = $this->model->findOrFail($id);
            if($data->update($input)){
                activity()
                    ->causedBy($this->user())
                    ->performedOn($data)
                    ->withProperties([
                        'icon'  => $this->icon,
                        'title' => $this->title,
                        'type'  => 'edit'
                    ])->log('Edit '.$this->title);

                DB::commit();
                alert()->success('Berhasil', 'Data telah berhasil disimpan');
                return redirect()->route($this->route.'index');
            }

            alert()->error('Gagal', 'Data telah gagal disimpan');
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            alert()->error('Gagal', $e->getMessage());
            return redirect()->back();
        }
    }

    public function destroy($id){
        $data = $this->model->findOrFail($id);
        if($data->delete()){
            activity()
                ->causedBy($this->user())
                ->withProperties([
                    'icon'  => $this->icon,
                    'title' => $this->title,
                    'type'  => 'delete'
                ])->log('Delete '.$this->title);

            alert()->success('Berhasil', 'Data telah berhasil dihapus');
            return redirect()->route($this->route.'index');
        }

        alert()->error('Gagal', 'Data telah gagal dihapus');
        return redirect()->back();
    }

    public function detail(Request $request, $id)
    {
        $datas = $this->model->findOrFail($id);
        if($request->ajax()) {
            $data = Question::with('examination')->where('examination_id', $datas)->get();
            return Datatables::of($data)->make(true);
        };


        $exam_id = $request->id;
        $exam = Examination::where('id', $request->id)->first();
        $question = Question::where('examination_id', $request->id)->get();
        $this->data['subjects'] = Subject::orderBy('name', 'ASC')->get();
        $this->data['teachers'] = Teachers::orderBy('name', 'ASC')->get();
        // $this->data['data'] = Examination::orderBy('name', 'ASC');
        $this->data['abjad'] = Question::abjad();

        return view($this->view.'detail', $this->data, compact('exam_id', 'exam', 'question'));
    }

    public function dataDetailSoal(Request $request)
    {
        // $datas = $this->model->findOrFail($id);
        // dd($id);
        // $datas = Question::findOrFail($id);
        if($request->ajax()) {
            $data = Question::where('examination_id', '=', $request->id)->get();
            return Datatables::of($data)->make(true);
        };


        // $exam_id = $request->id;
        // $exam = Examination::where('id', $request->id)->first();
        // $question = Question::where('examination_id', $request->id)->get();
        // $this->data['subjects'] = Subject::orderBy('name', 'ASC')->get();
        // $this->data['teachers'] = Teachers::orderBy('name', 'ASC')->get();
        // // $this->data['data'] = Examination::orderBy('name', 'ASC');
        // $this->data['abjad'] = Question::abjad();

        // return view($this->view.'detail', $this->data, compact('exam_id', 'exam', 'question'));
    }

    public function storeQuestion(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $input = $request->all();
            // dd($input);
                $data = Question::create($input);
            if($data){
                activity()
                    ->causedBy($this->user())
                    ->withProperties([
                        'icon'  => $this->icon,
                        'title' => $this->title,
                        'type'  => 'create'
                    ])->log('Create '.$this->title);

                DB::commit();
                alert()->success('Berhasil', 'Data telah berhasil disimpan');
                return redirect()->route($this->route.'index');
            }

            alert()->error('Gagal', 'Data telah gagal disimpan');
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            alert()->error('Gagal', $e->getMessage());
            return redirect()->back();
        }
    }
}
