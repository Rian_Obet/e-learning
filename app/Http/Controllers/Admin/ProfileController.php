<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\User;

use Datatables;
use View, DB;
use UploadImg;

class ProfileController extends Controller
{
    protected $title = 'Profile';
    protected $view  = 'admin.profile.';
    protected $route = 'admin.profile.';
    protected $path = 'users/';
    protected $icon = 'ft-user';

    public function __construct(){
        View::share('route', $this->route);
        View::share('title', $this->title);
        View::share('view', $this->view);
    }

    protected function user(){
        return auth()->user();
    }

    public function index(){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['List '.$this->title, null]
        ]);
        $user = $this->user();

        return view($this->view.'index', compact('user'));
    }

    public function store(Request $req){
        DB::beginTransaction();
        try {
            $input = $req->only(['name', 'password']);
            if($input['password']){
                $input['password'] = bcrypt($input['password']);
            }else{
                unset($input['password']);
            }

            $img = $req->file('image');
            if(!is_null($img)){
                $upload = new UploadImg($img);
                $upload->setPath($this->path);
                $input['image'] = $upload->getFilename();
            }

            $data = $this->user();
            if($data->update($input)){
                if(!is_null($img)) $upload->upload();

                activity()
                    ->causedBy($this->user())
                    ->performedOn($data)
                    ->withProperties([
                        'icon' => $this->icon,
                        'title' => $this->title,
                        'type' => 'edit'
                    ])->log('Edit '.$this->title);

                DB::commit();
                alert()->success('Berhasil', 'Data telah berhasil disimpan');
                return redirect()->route($this->route.'index');
            }

            alert()->error('Gagal', 'Data telah gagal disimpan');
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            alert()->error('Gagal', $e->getMessage());
            return redirect()->back();
        }
    }
}
