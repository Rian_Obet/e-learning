<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Datatables;
use UploadImg;
use View, DB;

class RoleController extends Controller
{
    protected $model;
    protected $title = 'Permission';
    protected $view  = 'admin.role.';
    protected $route = 'admin.role.';
    protected $permission = 'Role ';
    protected $icon = 'ft-shield';

    public function __construct(Role $model){
        $this->model = $model;

        View::share('route', $this->route);
        View::share('title', $this->title);
        View::share('view', $this->view);
        View::share('permission', $this->permission);

        $this->middleware('permission:'.$this->permission.'index')->only('index', 'show');
        $this->middleware('permission:'.$this->permission.'create')->only('create', 'store');
        $this->middleware('permission:'.$this->permission.'edit')->only('edit', 'update');
        $this->middleware('permission:'.$this->permission.'delete')->only('destroy');
        $this->middleware('permission:'.$this->permission.'configure')->only('configureForm', 'configure');
    }

    protected function user(){
        return auth()->user();
    }

    public function index(Request $req){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['List '.$this->title, null]
        ]);

        if($req->ajax()) {
            $data = $this->model->orderBy('name', 'asc');
            return Datatables::of($data)->make(true);
        };

        return view($this->view.'index');
    }

    public function create(){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['Create '.$this->title, null]
        ]);

        return view($this->view.'create');
    }

    public function store(Request $req){
        DB::beginTransaction();
        try {
            $input = $req->only(['name']);

            $data = $this->model->create($input);
            if($data){
                activity()
                    ->causedBy($this->user())
                    ->performedOn($data)
                    ->withProperties([
                        'icon'  => $this->icon,
                        'title' => $this->title,
                        'type'  => 'create'
                    ])->log('Create '.$this->title);

                DB::commit();
                alert()->success('Berhasil', 'Data telah berhasil disimpan');
                return redirect()->route($this->route.'index');
            }

            alert()->error('Gagal', 'Data telah gagal disimpan');
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            alert()->error('Gagal', $e->getMessage());
            return redirect()->back();
        }
    }

    public function show($id){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['Detail '.$this->title, null]
        ]);
        $data = $this->model->findOrFail($id);

        return view($this->view.'show', compact('data'));
    }

    public function edit($id){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['Edit '.$this->title, null]
        ]);
        $data = $this->model->findOrFail($id);

        return view($this->view.'edit', compact('data', 'id'));
    }

    public function update(Request $req, $id){
        DB::beginTransaction();
        try {
            $input = $req->only(['name']);

            $data = $this->model->findOrFail($id);
            if($data->update($input)){
                activity()
                    ->causedBy($this->user())
                    ->performedOn($data)
                    ->withProperties([
                        'icon'  => $this->icon,
                        'title' => $this->title,
                        'type'  => 'edit'
                    ])->log('Edit '.$this->title);

                DB::commit();
                alert()->success('Berhasil', 'Data telah berhasil disimpan');
                return redirect()->route($this->route.'index');
            }

            alert()->error('Gagal', 'Data telah gagal disimpan');
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            alert()->error('Gagal', $e->getMessage());
            return redirect()->back();
        }
    }

    public function configureForm($id){
        View::share('breadcrumbs', [
            [$this->title, route($this->route.'index')],
            ['Edit '.$this->title, null]
        ]);

        $data = $this->model->findOrFail($id);
        $isPermission = $data->permissions->pluck('id')->toArray();
        $permissions = Permission::orderBy('name', 'asc')->get();

        return view($this->view.'configure', compact('id', 'data', 'permissions', 'isPermission'));
    }

    public function configure(Request $req, $id){
        DB::beginTransaction();
        try {
            $role = $this->model->findOrFail($id);
            $isPermission = $role->permissions->pluck('id')->toArray();
            $input = $req->permissions;
            $del = json_decode($req->delete_permissions);

            if(count($del)){
                $dataDel = [];
                foreach($del as $r){
                    $perm = Permission::find($r);
                    if($perm) $role->revokePermissionTo($perm);
                }
            }

            if($input){
                $dataPermission = [];
                foreach($input as $r){
                    if(!in_array($r, $isPermission)){
                        $perm = Permission::find($r);
                        if($perm) $dataPermission[] = $perm;
                    }
                }
                if(count($dataPermission)) $role->givePermissionTo($dataPermission);
            }

            activity()
                ->causedBy($this->user())
                ->performedOn($role)
                ->withProperties([
                    'icon'  => $this->icon,
                    'title' => $this->title,
                    'type'  => 'configure'
                ])->log('Configure '.$this->title);

            DB::commit();

            alert()->success('Berhasil', 'Perubahan berhasil disimpan');
            return redirect()->route($this->route.'index');
        } catch (\Exception $e) {
            DB::rollback();

            alert()->error('Gagal', $e->getMessage());
            return redirect()->back();
        }
    }

    public function destroy($id){
        $data = $this->model->findOrFail($id);
        if($data->delete()){
            activity()
                ->causedBy($this->user())
                ->withProperties([
                    'icon'  => $this->icon,
                    'title' => $this->title,
                    'type'  => 'delete'
                ])->log('Delete '.$this->title);

            alert()->success('Berhasil', 'Data telah berhasil dihapus');
            return redirect()->route($this->route.'index');
        }

        alert()->error('Gagal', 'Data telah gagal dihapus');
        return redirect()->back();
    }
}
