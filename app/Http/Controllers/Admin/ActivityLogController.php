<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Datatables;
use View, DB;
use Dashboard;

use Spatie\Activitylog\Models\Activity;

class ActivityLogController extends Controller
{
    protected $title = 'Activity log';
    protected $view  = 'admin.activity-log.';
    protected $route = 'admin.activity-log.';

    public function __construct(){
        View::share('route', $this->route);
        View::share('title', $this->title);
        View::share('view', $this->view);
    }

    protected function user(){
        return auth()->user();
    }

    public function index(){
        View::share('breadcrumbs', [
            ['List '.$this->title, null]
        ]);
        $user = $this->user();

        return view($this->view.'index', compact('user'));
    }

    public function api(Request $req){
        $id = $this->user()->id; 
        $activities = Activity::select([
            'log_name',
            'description',
            'causer_id',
            'causer_type',
            'properties',
            'created_at'
        ])
        ->where([ 'causer_type' => 'App\\Models\\User' ])
        ->orderBy('created_at', 'desc')
        ->paginate(5);

        $data = [];
        foreach($activities as $r){
            $data[] = [
                'log_name'    => $r->log_name,
                'description' => $r->description,
                'causer'      => $r->causer,
                'properties'  => json_decode($r->properties),
                'created_at'  => Dashboard::indonesiaDateFormat($r->created_at, true),
            ];
        }

        $res = [
            'success' => true, 
            'data' => $data,
            'next_page' => $activities->nextPageUrl()
        ];

        return $res;
    }
}
