<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\Classes;
use Auth, View;

class DashboardController extends Controller
{
    protected $view  = 'student.';
    protected $route = 'student.';

    public function __construct(Student $model){
        $this->model = $model;

        View::share('route', $this->route);
        View::share('view', $this->view);
        View::share('class', Classes::all());

    }

    protected function guard(){
        return Auth::guard();
    }

    public function index(){
        $title = 'Siswa';
      
        $data = Auth::guard('student')->user();

        return view($this->view.'dashboard', compact('data', 'title'));
    }
}
