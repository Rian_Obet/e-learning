<?php

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;

class TraitMakeCommand extends GeneratorCommand{
    protected $signature   = 'make:trait {name}';
    protected $description = 'Nyieun trait';
    protected $type        = 'Traits';

    protected function getStub(){
        return __DIR__.'/stubs/trait.stub';
    }

    protected function getArguments(){
        return [
            ['name', InputArgument::REQUIRED, 'The name of the command.'],
        ];
    }

    protected function getDefaultNamespace($rootNamespace){
        return $rootNamespace.'\Traits';
    }
}
