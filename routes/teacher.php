<?php
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'guest'], function(){
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login')->name('login.action');
  });

  Route::group(['middleware' => 'auth:teacher'], function(){

    Route::get('home', 'DashboardController@index')->name('index');
    Route::get('profile', 'DashboardController@profile')->name('profile');
    Route::post('logout', 'LoginController@logout')->name('logout');

  // Examination
  Route::resource('examination', 'ExaminationController');

  // QUESTION
  Route::resource('question', 'QuestionController');

  // Package Question
  Route::resource('question_package', 'QuestionPackageController');
  Route::get('question_package/{id}/configure', 'QuestionPackage@configureForm')->name('question_package.configure');
  Route::post('question_package/{id}/configure', 'QuestionPackage@configure')->name('question_package.configure.action');

});
