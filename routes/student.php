<?php 
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'guest'], function(){
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login')->name('login.action');
  });
  
  Route::group(['middleware' => 'auth:student'], function(){
  
    Route::get('', 'DashboardController@index')->name('index');
    Route::post('logout', 'LoginController@logout')->name('logout');

  // EXAMINATION PACKAGE
  Route::resource('examination', 'ExaminationController');
 
  // QUESTION PACKAGE
  // Route::resource('question', 'QuestionController');
 
});  