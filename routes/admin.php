<?php
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'guest'], function(){
  Route::get('login', 'LoginController@showLoginForm')->name('login');
  Route::post('login', 'LoginController@login')->name('login.action');
});

Route::group(['middleware' => 'auth'], function(){
  Route::get('', 'DashboardController@index')->name('index');
  Route::post('logout', 'LoginController@logout')->name('logout');

  // profile
  Route::resource('profile', 'ProfileController')->only(['index', 'store']);

  // activity log
  Route::get('activity-log', 'ActivityLogController@index')->name('activity-log.index');
  Route::get('activity-log/api', 'ActivityLogController@api')->name('activity-log.api');

  // user
  Route::get('user/{id}/configure', 'UserController@configureForm')->name('user.configure');
  Route::post('user/{id}/configure', 'UserController@configure')->name('user.configure.action');
  Route::resource('user', 'UserController');

  // permission
  Route::resource('permission', 'PermissionController')->except('show');

  // role
  Route::get('role/{id}/configure', 'RoleController@configureForm')->name('role.configure');
  Route::post('role/{id}/configure', 'RoleController@configure')->name('role.configure.action');
  Route::resource('role', 'RoleController')->except('show');

  // teacher
  Route::resource('teacher', 'TeacherController');

  // STUDENT
  Route::resource('student', 'StudentController');

  // Examination
  Route::resource('examination', 'ExaminationController');
  Route::get('examination/{id}/detail', 'ExaminationController@detail')->name('examination.detail');
  Route::get('/get-detail-soal', 'ExaminationController@dataDetailSoal')->name('examination.get-detail-soal');
	Route::post('/examination/save-question-examination', 'ExaminationController@storeQuestion')->name('examination.save-question-examination');



  // Question
  Route::resource('question', 'QuestionController');
  Route::post('/images', 'QuestionController@uploadImage')->name('post.image');

  // Package Question
  Route::resource('question_package', 'QuestionPackageController');

  // Lesson
  Route::resource('subject', 'SubjectController');

  // Class
  Route::resource('class', 'ClassesController');
});
